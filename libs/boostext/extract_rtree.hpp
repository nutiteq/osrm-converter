// Boost.Geometry Index
//
// R-tree iterator visitor implementation
//
// Copyright (c) 2011-2015 Adam Wulkiewicz, Lodz, Poland.
//
// Use, modification and distribution is subject to the Boost Software License,
// Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOSTEXT_EXTRACT_RTREE_HPP
#define BOOSTEXT_EXTRACT_RTREE_HPP

#include <vector>

namespace boost { namespace geometry { namespace index {
    
    template <typename Value, typename Box>
    struct extracted_rtree_node {
        bool leaf;
        Box box;
        std::vector<std::size_t> children;
        std::vector<Value> values;
        
        extracted_rtree_node(bool leaf, Box const& box) : leaf(leaf), box(box), children(), values()
        {
        }
    };
    
    namespace detail { namespace rtree { namespace visitors {
        
        template <typename Value, typename Options, typename Translator, typename Box, typename Allocators>
        class extract_iterator
        : public rtree::visitor<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag, true>::type
        {
        public:
            typedef typename rtree::node<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag>::type node;
            typedef typename rtree::internal_node<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag>::type internal_node;
            typedef typename rtree::leaf<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag>::type leaf;
            
            typedef extracted_rtree_node<Value, Box> rtree_node;
            
            typedef typename Allocators::size_type size_type;
            typedef typename Allocators::const_reference const_reference;
            typedef typename Allocators::node_pointer node_pointer;
            
            typedef typename rtree::elements_type<internal_node>::type::const_iterator internal_iterator;
            typedef typename rtree::elements_type<leaf>::type leaf_elements;
            typedef typename rtree::elements_type<leaf>::type::const_iterator leaf_iterator;
            
            inline extract_iterator(Translator const& tr) : m_rtree_nodes(), m_tr(tr)
            {
            }
            
            inline void operator()(internal_node const& n)
            {
                typedef typename rtree::elements_type<internal_node>::type elements_type;
                elements_type const& elements = rtree::elements(n);
                Box box = rtree::elements_box<Box>(elements.begin(), elements.end(), m_tr);
                std::size_t index = m_rtree_nodes.size();
                m_rtree_nodes.push_back(rtree_node(false, box));
                for (typename elements_type::const_iterator it = elements.begin();
                     it != elements.end(); ++it)
                {
                    m_rtree_nodes[index].children.push_back(m_rtree_nodes.size());
                    rtree::apply_visitor(*this, *it->second);
                }
            }
            
            inline void operator()(leaf const& l)
            {
                typedef typename rtree::elements_type<leaf>::type elements_type;
                elements_type const& elements = rtree::elements(l);
                Box box = rtree::elements_box<Box>(elements.begin(), elements.end(), m_tr);
                std::size_t index = m_rtree_nodes.size();
                m_rtree_nodes.push_back(rtree_node(true, box));
                for (typename elements_type::const_iterator it = elements.begin();
                     it != elements.end(); ++it)
                {
                    m_rtree_nodes[index].values.push_back(*it);
                }
            }
            
            inline const std::vector<rtree_node> rtree_nodes() const
            {
                return m_rtree_nodes;
            }
            
        private:
            std::vector<rtree_node> m_rtree_nodes;
            Translator m_tr;
        };
        
    }}} // namespace detail::rtree::visitors
    
    template <typename Value, typename Options, typename IndexableGetter, typename EqualTo, typename Allocator>
    std::vector<extracted_rtree_node<Value, typename detail::rtree::utilities::view<rtree<Value, Options, IndexableGetter, EqualTo, Allocator> >::box_type > >
    extract_rtree(rtree<Value, Options, IndexableGetter, EqualTo, Allocator> const& tree)
    {
        typedef rtree<Value, Options, IndexableGetter, EqualTo, Allocator> rtree_type;
        
        typedef typename detail::rtree::utilities::view<rtree_type> view;
        typedef typename view::translator_type translator_type;
        typedef typename view::value_type value_type;
        typedef typename view::options_type options_type;
        typedef typename view::box_type box_type;
        typedef typename view::allocators_type allocators_type;
        
        detail::rtree::utilities::view<rtree_type> tree_view(tree);

        detail::rtree::visitors::extract_iterator<value_type, options_type, translator_type, box_type, allocators_type> extract_v(tree_view.translator());
        
        tree_view.apply_visitor(extract_v);
        return extract_v.rtree_nodes();
    }
    
}}} // namespace boostext::geometry::index

#endif // BOOSTEXT_EXTRACT_RTREE_HPP
