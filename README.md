## About

OSRM Converter is a tool for creating Nutiteq Offline Routing Packages from OSRM prepared data.
The resulting packages are block-based, highly-compressed and well suited for offline mobile usage.

## Building

OSRM Converter has several prerequisites:

- Modern C++ compiler (g++ 4.8+, VS2015, clang3.4+)
- CMake build system
- Boost 1.59 (version number IS IMPORTANT!)
- stxxl library 1.4.1

The usual steps for building are:

- download boost, unpack it in osrm-converter directory, rename boost_1_59_0 to boost, run `bootstrap.sh`, run `./b2 stage` in boost directory
- download stxxl, unpack it in osrm-converter directory, rename stxxl-1.4.1 to stxxl
- create build directory (`mkdir build`)
- execute CMake in build directory (`cd build`, `cmake -DCMAKE_BUILD_TYPE=Release ..`)
- build (`make`)

## Usage

OSRM Converter requires graph prepared by OSRM (osrm-prepare output) and packages.json file.
The graph should cover all the packages in the *packages.json* file (usually whole planet).
To create all routing packages corresponding to *packages.json* file, execute
`osrm-converter planet-latest.osrm packages.json`.

OSRM Converter requires about 100GB of temporary storage for all packages.
Final *.nutigraph* packages are almost a magnitude smaller.
For building planet, the converter needs about 64GB of RAM (or swap space).
Building planet packages can take about 4-8 hours on a fast quadcode server.

Note: it is VERY IMPORTANT to use the same C++ compiler
version/target architecture for OSRM Converter as for OSRM Backend.
OSRM serializes data in a specific way that is specific to compiler/target architecture.
