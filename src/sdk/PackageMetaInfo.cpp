#ifdef _NUTI_PACKAGEMANAGER_SUPPORT

#include "PackageMetaInfo.h"

#include <rapidjson/rapidjson.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/document.h>

namespace Nuti {

	PackageMetaInfo::PackageMetaInfo(const std::string& jsonValue) : _jsonValue(jsonValue), _value(std::make_shared<rapidjson::Document>()) {
		if (std::static_pointer_cast<rapidjson::Document>(_value)->Parse<rapidjson::kParseDefaultFlags>(jsonValue.c_str()).HasParseError()) {
			throw std::runtime_error("Error while parsing meta info");
		}
	}

	PackageMetaInfo::PackageMetaInfo(const rapidjson::Value& value) : _jsonValue(), _value(std::make_shared<rapidjson::Document>()) {
		rapidjson::StringBuffer metaInfo;
		rapidjson::Writer<rapidjson::StringBuffer> writer(metaInfo);
		value.Accept(writer);
		_jsonValue = metaInfo.GetString();
		if (std::static_pointer_cast<rapidjson::Document>(_value)->Parse<rapidjson::kParseDefaultFlags>(_jsonValue.c_str()).HasParseError()) {
			throw std::runtime_error("Error while parsing meta info");
		}
	}

	PackageMetaInfoType::PackageMetaInfoType PackageMetaInfo::getType() const {
		if (_value->IsString()) {
			return PackageMetaInfoType::PACKAGE_META_INFO_TYPE_STRING;
		}
		if (_value->IsFalse() || _value->IsTrue()) {
			return PackageMetaInfoType::PACKAGE_META_INFO_TYPE_BOOL;
		}
		if (_value->IsInt() || _value->IsUint() || _value->IsInt64() || _value->IsUint64()) {
			return PackageMetaInfoType::PACKAGE_META_INFO_TYPE_INTEGER;
		}
		if (_value->IsNumber()) {
			return PackageMetaInfoType::PACKAGE_META_INFO_TYPE_DOUBLE;
		}
		if (_value->IsArray()) {
			return PackageMetaInfoType::PACKAGE_META_INFO_TYPE_ARRAY;
		}
		if (_value->IsObject()) {
			return PackageMetaInfoType::PACKAGE_META_INFO_TYPE_MAP;
		}
		return PackageMetaInfoType::PACKAGE_META_INFO_TYPE_NULL;
	}

	std::string PackageMetaInfo::getString() const {
		if (_value->IsString()) {
			return std::string(_value->GetString(), _value->GetStringLength());
		}
		return std::string();
	}

	bool PackageMetaInfo::getBool() const {
		if (_value->IsFalse()) {
			return false;
		}
		if (_value->IsTrue()) {
			return true;
		}
		return false;
	}

	long long PackageMetaInfo::getLong() const {
		if (_value->IsInt()) {
			return _value->GetInt();
		}
		if (_value->IsUint()) {
			return _value->GetUint();
		}
		if (_value->IsInt64()) {
			return _value->GetInt64();
		}
		if (_value->IsUint64()) {
			return _value->GetUint64();
		}
		return 0;
	}

	double PackageMetaInfo::getDouble() const {
		if (_value->IsNumber()) {
			return _value->GetDouble();
		}
		return 0.0;
	}

	int PackageMetaInfo::getArraySize() const {
		if (_value->IsArray()) {
			return static_cast<int>(_value->Size());
		}
		return 0;
	}

	std::shared_ptr<PackageMetaInfo> PackageMetaInfo::getArrayElement(int idx) const {
		if (_value->IsArray()) {
			if (idx < static_cast<int>(_value->Size())) {
				return std::make_shared<PackageMetaInfo>((*_value)[idx]);
			}
		}
		return std::shared_ptr<PackageMetaInfo>();
	}
	
	std::vector<std::string> PackageMetaInfo::getMapKeys() const {
		std::vector<std::string> keys;
		if (_value->IsObject()) {
			for (rapidjson::Value::ConstMemberIterator jit = _value->MemberBegin(); jit != _value->MemberEnd(); jit++) {
				keys.push_back(jit->name.GetString());
			}
		}
		return keys;
	}
	
	std::shared_ptr<PackageMetaInfo> PackageMetaInfo::getMapElement(const std::string& key) const {
		if (_value->IsObject()) {
			if (_value->HasMember(key.c_str())) {
				return std::make_shared<PackageMetaInfo>((*_value)[key.c_str()]);
			}
		}
		return std::shared_ptr<PackageMetaInfo>();
	}

}

#endif
