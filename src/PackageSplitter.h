#pragma once

#include "OSRMGraph.h"

#include "sdk/PackageTileMask.h"

#include <memory>
#include <mutex>
#include <string>
#include <utility>
#include <map>
#include <set>
#include <vector>

#include <stxxl/vector>

#include <cglib/vec.h>
#include <cglib/bbox.h>

namespace Nuti { namespace Routing {
    class PackageSplitter {
    public:
        using NodeId = OSRMGraph::NodeId;
        using PointId = OSRMGraph::PointId;
        using NodeIdVector = stxxl::VECTOR_GENERATOR<NodeId, 4, 8, 1 << 18>::result;
        
        PackageSplitter() = delete;
        PackageSplitter(const OSRMGraph& graph, const std::map<std::string, std::shared_ptr<PackageTileMask>>& packageTileMaskMap);

        void build();

        const NodeIdVector& getPackageNodeIds(const std::string& packageId) const;

    protected:
        using Node = OSRMGraph::Node;
        using Edge = OSRMGraph::Edge;
        using EdgeIterator = OSRMGraph::EdgeIterator;
        using Point = OSRMGraph::Point;
        using WMPos = cglib::vec2<double>;
        using WMBounds = cglib::bounding_box<double, 2>;
        
        std::vector<WMPos> getNodeGeometry(NodeId nodeId) const;

        static WMPos getWMPos(const Point& point);

        const OSRMGraph& _graph;
        
        const std::map<std::string, std::shared_ptr<PackageTileMask>> _packageTileMaskMap;

        std::map<std::string, NodeIdVector> _packageNodeMap;
        mutable std::mutex _packageNodeMapMutex;
 
        static const double COORDINATE_SCALE;
    };
} }
