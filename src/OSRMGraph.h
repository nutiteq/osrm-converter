#pragma once

#include <string>
#include <memory>
#include <mutex>
#include <array>
#include <vector>
#include <unordered_map>

#include <stxxl/vector>

#include <cglib/vec.h>

namespace Nuti { namespace Routing {
    class OSRMGraph {
    public:
        using NodeId = unsigned int;
        using PointId = unsigned int;
        using GeometryId = unsigned int;
        using NameId = unsigned int;
        using EdgeIterator = std::size_t;

        struct Point {
            int lat = 0;
            int lon = 0;

            Point() = default;
        };

        struct NodeData {
            PointId startPointId = -1;
            GeometryId packedGeometryId = -1;
            bool geometryReversed = false;
            NameId nameId = -1;
            unsigned int weight = 0;
            unsigned char travelMode = 0;

            NodeData() = default;
        };

        struct Node {
            EdgeIterator firstEdge = 0;
            EdgeIterator lastEdge = 0;
            NodeData nodeData;

            Node() = default;
        };

        struct EdgeData {
            unsigned char turnInstruction = 0;

            EdgeData() = default;
        };

        struct Edge {
            NodeId targetNodeId = -1;
            NodeId contractedNodeId = -1;
            unsigned int weight = 0;
            bool forward = false;
            bool backward = false;
            EdgeData edgeData;

            Edge() = default;
        };

        explicit OSRMGraph(const std::string& fileName);

        std::size_t getNodeCount() const;
        const Node& getNode(NodeId nodeId) const;
        const Edge& getEdge(EdgeIterator edgeId) const;
        const std::string& getName(NameId nameId) const;
        const Point& getPoint(PointId pointId) const;
        std::vector<PointId> getGeometry(GeometryId geometryId) const;

    private:
        using NodeVector = std::vector<Node>;
        using EdgeVector = std::vector<Edge>;
        using GeometryIndexVector = std::vector<std::size_t>;
        using PointIdVector = std::vector<PointId>;
        using PointVector = std::vector<Point>;
        using NodeDataVector = std::vector<NodeData>;
        using EdgeDataVector = std::vector<EdgeData>;

        void importNames();
        void importPoints();
        void importGeometry();
        void importNodes(NodeDataVector& nodeDataMap);
        void importEdges(EdgeDataVector& edgeDataMap);
        void importGraph();

        std::vector<std::string> _names;
        mutable std::mutex _namesMutex;
        
        GeometryIndexVector _geometryIndices;
        PointIdVector _geometryPointIds;
        PointVector _geometryPoints;
        mutable std::mutex _geometryMutex;

        NodeVector _nodes;
        mutable std::mutex _nodesMutex;
        
        EdgeVector _edges;
        mutable std::mutex _edgesMutex;

        const std::string _fileName;
    };
} }
