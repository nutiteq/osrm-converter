#pragma once

#include "OSRMGraph.h"
#include "PackageSplitter.h"

#include "sdk/PackageTileMask.h"

#include <memory>
#include <string>
#include <utility>
#include <map>
#include <set>
#include <vector>
#include <unordered_set>

#include <boost/program_options.hpp>

#include <cglib/vec.h>
#include <cglib/bbox.h>

#include <stdext/bitstream.h>
#include <stdext/eiff_file.h>

namespace Nuti { namespace Routing {
    class RoutingGraphBuilder {
    public:
        struct Options {
            unsigned maxNameBlockSize = 0;
            unsigned maxNamesPerBlock = 256;
            unsigned maxGeometryBlockSize = 0;
            unsigned maxGeometriesPerBlock = 512;
            unsigned maxNodeBlockSize = 0;
            unsigned maxNodesPerBlock = 512;
            unsigned rtreeNodesPerBlock = 512;
            unsigned globalNodesPerBlock = 512;

            Options() = default;

            void setVariables(const boost::program_options::variables_map &vm) {
                maxNameBlockSize = vm["max-name-block-size"].as<unsigned>();
                maxNamesPerBlock = vm["max-names-per-block"].as<unsigned>();
                maxGeometryBlockSize = vm["max-geometry-block-size"].as<unsigned>();
                maxGeometriesPerBlock = vm["max-geometries-per-block"].as<unsigned>();
                maxNodeBlockSize = vm["max-node-block-size"].as<unsigned>();
                maxNodesPerBlock = vm["max-nodes-per-block"].as<unsigned>();
                rtreeNodesPerBlock = vm["rtree-nodes-per-block"].as<unsigned>();
                globalNodesPerBlock = vm["global-nodes-per-block"].as<unsigned>();
            }

            static boost::program_options::options_description getDescription() {
            	namespace po = boost::program_options;
            	Options opt;
            	po::options_description desc("Converter options");
            	desc.add_options()
            		("max-name-block-size", po::value<unsigned>()->default_value(opt.maxNameBlockSize), "maximum name block size (0 to disable)")
            		("max-names-per-block", po::value<unsigned>()->default_value(opt.maxNamesPerBlock), "maximum names per block")
            		("max-geometry-block-size", po::value<unsigned>()->default_value(opt.maxGeometryBlockSize), "maximum geometry block size (0 to disable)")
            		("max-geometries-per-block", po::value<unsigned>()->default_value(opt.maxGeometriesPerBlock), "maximum geometries per block")
            		("max-node-block-size", po::value<unsigned>()->default_value(opt.maxNodeBlockSize), "maximum node block size (0 to disable)")
            		("max-nodes-per-block", po::value<unsigned>()->default_value(opt.maxNodesPerBlock), "maximum nodes per block")
                    ("rtree-nodes-per-block", po::value<unsigned>()->default_value(opt.rtreeNodesPerBlock), "R-tree nodes per block")
                    ("global-nodes-per-block", po::value<unsigned>()->default_value(opt.globalNodesPerBlock), "global nodes per block")
            	;
            	return desc;
            }
        };

        using BlockId = std::size_t;
        using BlockIndex = std::pair<BlockId, std::size_t>;
        using NodeId = OSRMGraph::NodeId;
        using PointId = OSRMGraph::PointId;
        using GeometryId = OSRMGraph::GeometryId;
        using NameId = OSRMGraph::NameId;
        using RTreeNodeId = std::size_t;

        RoutingGraphBuilder() = delete;
        RoutingGraphBuilder(const Options& options, const OSRMGraph& graph, const PackageSplitter::NodeIdVector& nodeIds, const std::string& packageId, const std::string& fileName);

        void build();
        void link(const RoutingGraphBuilder& builder);
        void save();
        
    private:
        using EdgeIterator = OSRMGraph::EdgeIterator;
        using Node = OSRMGraph::Node;
        using Edge = OSRMGraph::Edge;
        using Point = OSRMGraph::Point;
        using WGSPos = cglib::vec2<double>;
        using WGSBounds = cglib::bounding_box<double, 2>;

        struct GlobalNode {
            std::string package;
            BlockId blockIndex = 0;
            std::size_t nodeIndex = 0;
            
            GlobalNode() = default;
        };
        
        struct RTreeNode {
            bool leaf = false;
            std::vector<std::pair<WGSBounds, RTreeNodeId>> children;
            std::vector<std::pair<WGSBounds, BlockId>> nodeBlocks;
            
            RTreeNode() = default;
        };

        struct NameBlock {
            std::vector<std::string> names;

            NameBlock() = default;
        };

        struct GeometryBlock {
            std::vector<std::vector<PointId>> geometries;
            int minLat = std::numeric_limits<int>::max();
            int minLon = std::numeric_limits<int>::max();
            int maxLat = std::numeric_limits<int>::min();
            int maxLon = std::numeric_limits<int>::min();

            GeometryBlock() = default;
        };

        struct NodeBlock {
            std::vector<NodeId> nodes;
            std::array<int, 32> weightBitsHistogram { };
            BlockId minNameBlockId = std::numeric_limits<BlockId>::max();
            BlockId minGeometryBlockId = std::numeric_limits<BlockId>::max();

            NodeBlock() = default;
        };
        
        struct GlobalNodeBlock {
            std::vector<std::string> packages;
            std::vector<std::pair<NodeId, std::vector<GlobalNode>>> globalNodes;
            
            GlobalNodeBlock() = default;
        };
        
        struct RTreeNodeBlock {
            std::vector<RTreeNode> nodes;
            int minLat = std::numeric_limits<int>::max();
            int minLon = std::numeric_limits<int>::max();
            int maxLat = std::numeric_limits<int>::min();
            int maxLon = std::numeric_limits<int>::min();
            
            RTreeNodeBlock() = default;
        };
        
        void buildHeader();
        void addChunk(eiff::chunk::tag_type tag, const std::shared_ptr<std::fstream>& file);

        BlockIndex allocateNameId(NameId nameId);
        void encodeNameBlock(const NameBlock& block, bitstreams::output_bitstream& bs) const;
        void writeNameBlock();
        void buildNames();

        BlockIndex allocateGeometryId(PointId startPointId, GeometryId geometryId);
        void encodeGeometryBlock(const GeometryBlock& block, bitstreams::output_bitstream& bs) const;
        void writeGeometryBlock();
        void buildGeometry();

        BlockIndex allocateGlobalNodeId(NodeId globalNodeId);
        void encodeGlobalNodeBlock(const GlobalNodeBlock& block, bitstreams::output_bitstream& bs);
        void writeGlobalNodeBlock();
        void buildGlobalNodes();
        
        void addNodeId(NodeId nodeId);
        void encodeNodeBlock(const NodeBlock& block, BlockId blockId, bitstreams::output_bitstream& bs);
        void writeNodeBlock();
        void buildGraph();
        
        void addRTreeNode(const RTreeNode& node);
        void encodeRTreeNodeBlock(const RTreeNodeBlock& block, BlockId blockId, bitstreams::output_bitstream& bs);
        void writeRTreeNodeBlock();
        void buildRTree();
        
        std::vector<WGSPos> getNodeGeometry(NodeId nodeId) const;

        std::vector<NodeId> getTopologicalSpatialNodeOrdering() const;
        
        static std::shared_ptr<std::fstream> writeBlockedFile(const std::string& fileName, const std::vector<bitstreams::output_bitstream>& blocks);
        
        static cglib::bounding_box<int, 2> calculateBounds(const WGSBounds& bbox);

        static long long calculateHilbertCode(const WGSPos& pos, const WGSBounds& bbox);

        static WGSPos getWGSPos(const Point& point);

        static unsigned int encodeZigZagValue(int val);
        static unsigned char encodeTravelMode(unsigned char travelMode);

        std::unordered_map<NameId, BlockIndex> _nameBlockMap;
        std::vector<bitstreams::output_bitstream> _encodedNameBlocks;
        NameBlock _currentNameBlock;

        std::map<std::pair<PointId, GeometryId>, BlockIndex> _geometryBlockMap;
        std::vector<bitstreams::output_bitstream> _encodedGeometryBlocks;
        GeometryBlock _currentGeometryBlock;

        std::unordered_map<NodeId, BlockIndex> _nodeBlockMap;
        std::vector<bitstreams::output_bitstream> _encodedNodeBlocks;
        NodeBlock _currentNodeBlock;

        std::vector<NodeId> _globalNodeIds;
        std::unordered_map<NodeId, BlockIndex> _globalNodeBlockMap;
        std::unordered_map<NodeId, std::vector<GlobalNode>> _linkedGlobalNodeMap;
        std::vector<bitstreams::output_bitstream> _encodedGlobalNodeBlocks;
        GlobalNodeBlock _currentGlobalNodeBlock;
        
        std::vector<bitstreams::output_bitstream> _encodedRTreeNodeBlocks;
        RTreeNodeBlock _currentRTreeNodeBlock;
        
        std::map<std::string, eiff::chunk::tag_type> _fileChunkMap;

        const OSRMGraph& _graph;
        std::vector<NodeId> _nodeIds; // NOTE: sorted
        const Options _options;
        const std::string _packageId;
        const std::string _fileName;

        static const int VERSION;
 
        static const double COORDINATE_SCALE;
    };
} }
