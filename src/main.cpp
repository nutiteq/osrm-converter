#include "RoutingGraphBuilder.h"
#include "PackageSplitter.h"

#include "osrm/simple_logger.hpp"
#include "osrm/timing_util.hpp"

#include <iostream>
#include <fstream>

#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/bind.hpp>

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>

#if defined(USE_OPENMP)
#include <omp.h>
#endif

#if defined(WIN32) && !defined(NDEBUG)
#	include <crtdbg.h> 
#endif

namespace po = boost::program_options;
namespace fs = boost::filesystem;

std::string encodePackageId(const std::string& packageId, const std::string& graphFileName) {
    std::ifstream graphFile;
    graphFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        graphFile.open(graphFileName + ".hsgr", std::ios::binary | std::ios::in);
    }
    catch (const std::exception&) {
        throw std::runtime_error("Failed to open graph .hsgr file");
    }
    graphFile.seekg(0, std::ios::end);
    std::ifstream::pos_type graphFileSize = graphFile.tellg();

    std::stringstream str;
    str << packageId << "#" << static_cast<long long>(graphFileSize);
    return str.str();
}

std::map<std::string, std::shared_ptr<Nuti::PackageTileMask>> loadPackageTileMasks(const std::string& packageFileName) {
    std::ifstream packageFile;
    packageFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        packageFile.open(packageFileName, std::ios::binary | std::ios::in);
    }
    catch (const std::exception&) {
        throw std::runtime_error("Failed to open package file");
    }
    std::string packageJson;
    try {
        while (!packageFile.eof()) {
            char c = 0;
            packageFile.read(&c, 1);
            packageJson.append(1, c);
        }
    }
    catch (const std::exception&) {
        if (!packageFile.eof()) {
            throw std::runtime_error("Failed to read package file");
        }
    }
    rapidjson::Document packageListDoc;
    if (packageListDoc.Parse<rapidjson::kParseDefaultFlags>(packageJson.c_str()).HasParseError()) {
        throw std::runtime_error("Error while parsing packages file");
    }
    
    std::map<std::string, std::shared_ptr<Nuti::PackageTileMask>> packageTileMaskMap;
    for (rapidjson::Value::ValueIterator jit = packageListDoc["packages"].Begin(); jit != packageListDoc["packages"].End(); jit++) {
        rapidjson::Value& jsonPackageInfo = *jit;
        std::shared_ptr<Nuti::PackageTileMask> tileMask;
        if (jsonPackageInfo.HasMember("tile_mask")) {
            std::string id = jsonPackageInfo["id"].GetString();
            auto tileMask = std::make_shared<Nuti::PackageTileMask>(jsonPackageInfo["tile_mask"].GetString());
            packageTileMaskMap[id] = tileMask;
        }
    }
    return packageTileMaskMap;
}

std::map<std::string, std::shared_ptr<Nuti::PackageTileMask>> createTestPackageTileMasks() {
    std::map<std::string, std::shared_ptr<Nuti::PackageTileMask>> packageTileMaskMap;
    for (int i = 0; i < 2; i++) {
        std::vector<Nuti::PackageTileMask::Tile> tiles;
        Nuti::PackageTileMask::Tile tile(12, 2351 + i, (1 << 12) - 1 - 1225);
        while (tile.zoom >= 0) {
            tiles.push_back(tile);
            tile = Nuti::PackageTileMask::Tile(tile.zoom - 1, tile.x / 2, tile.y / 2);
        }
        tile = Nuti::PackageTileMask::Tile(12, 2351 + i, (1 << 12) - 1 - 1226);
        while (tile.zoom >= 0) {
            tiles.push_back(tile);
            tile = Nuti::PackageTileMask::Tile(tile.zoom - 1, tile.x / 2, tile.y / 2);
        }
        packageTileMaskMap.emplace(i == 0 ? "T1" : "T2", std::make_shared<Nuti::PackageTileMask>(tiles));
    }
    return packageTileMaskMap;
}

int main(int argc, char* argv[]) {
    using namespace Nuti;
    using namespace Nuti::Routing;
    
    LogPolicy::GetInstance().Unmute();

    po::options_description options;
    options.add(RoutingGraphBuilder::Options::getDescription());

    po::options_description hidden_options("Hidden options");
    hidden_options.add_options()
        ("input-files", po::value<std::vector<std::string> >(), "input files")
        ;

#if defined(USE_OPENMP)
    po::options_description openmp_options("OpenMP options");
    openmp_options.add_options()
        ("num-threads", po::value<int>()->default_value(-1), "number of threads to use (-1 for all available)")
        ;
    options.add(openmp_options);
#endif

    po::options_description cmdline_options;
    cmdline_options.add(options).add(hidden_options);

    po::positional_options_description file_options;
    file_options.add("input-files", -1);

    try {
        po::variables_map vm;
        store(po::command_line_parser(argc, argv).
            options(cmdline_options).positional(file_options).run(), vm);
        notify(vm);

#if defined(USE_OPENMP)
        if (vm["num-threads"].as<int>() > 0) {
            omp_set_num_threads(vm["num-threads"].as<int>());
        }
#endif

        std::vector<std::string> inputFiles;
        if (vm.count("input-files") >= 1) {
            inputFiles = vm["input-files"].as<std::vector<std::string> >();
        }
        if (inputFiles.size() < 2) {
            std::cerr << "Usage: osrm-convert [options] input.osrm packages.json" << std::endl;
            std::cerr << std::endl << options << std::endl;
            return -1;
        }

        RoutingGraphBuilder::Options options;
        options.setVariables(vm);
        
        TIMER_START(processing);

        std::map<std::string, std::shared_ptr<PackageTileMask>> packageTileMaskMap = loadPackageTileMasks(inputFiles[1]); // createTestPackageTileMasks();
        OSRMGraph graph(inputFiles[0]);

        PackageSplitter splitter(graph, packageTileMaskMap);
        splitter.build();

        TIMER_START(buildingPackages);
        SimpleLogger().Write() << "Building packages";

        std::vector<std::shared_ptr<RoutingGraphBuilder>> builders;
        for (auto it = packageTileMaskMap.begin(); it != packageTileMaskMap.end(); it++) {
            if (splitter.getPackageNodeIds(it->first).empty()) {
                SimpleLogger().Write() << "Skipping empty package " << it->first;
                continue;
            }
            const std::string& packageId = it->first;
            auto builder = std::make_shared<RoutingGraphBuilder>(options, graph, splitter.getPackageNodeIds(packageId), encodePackageId(packageId, inputFiles[0]), packageId);
            builders.push_back(builder);
        }
        int builderCount = static_cast<int>(builders.size());
        #pragma omp parallel for
        for (int i = 0; i < builderCount; i++) {
            builders[i]->build();
        }

        TIMER_STOP(buildingPackages);
        SimpleLogger().Write() << "Building finished after " << TIMER_SEC(buildingPackages) << " seconds";

        TIMER_START(linkingPackages);
        SimpleLogger().Write() << "Linking packages";

        for (const std::shared_ptr<RoutingGraphBuilder>& builder1 : builders) {
            for (const std::shared_ptr<RoutingGraphBuilder>& builder2 : builders) {
                if (builder1 != builder2) {
                    builder1->link(*builder2);
                }
            }
            builder1->save();
        }

        TIMER_STOP(linkingPackages);
        SimpleLogger().Write() << "Linking finished after " << TIMER_SEC(linkingPackages) << " seconds";

        TIMER_STOP(processing);
        SimpleLogger().Write() << "All done, " << TIMER_SEC(processing) << " seconds";
        return 0;
    }
    catch (const std::exception& ex) {
        std::cerr << "Exception: " << ex.what() << std::endl;
    }
    return -1;
}
