#include "RoutingGraphBuilder.h"

#include "util/Hilbert.h"
#include "util/Wgs84.h"

#include "osrm/simple_logger.hpp"
#include "osrm/timing_util.hpp"

#include <cstdint>
#include <queue>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <functional>
#include <numeric>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/index/rtree.hpp>

#include <boostext/extract_rtree.hpp>

namespace Nuti { namespace Routing {
    RoutingGraphBuilder::RoutingGraphBuilder(const Options& options, const OSRMGraph& graph, const PackageSplitter::NodeIdVector& nodeIds, const std::string& packageId, const std::string& fileName) :
        _graph(graph), _nodeIds(nodeIds.begin(), nodeIds.end()), _options(options), _packageId(packageId), _fileName(fileName)
    {
        std::sort(_nodeIds.begin(), _nodeIds.end());
    }

    void RoutingGraphBuilder::build() {
        TIMER_START(build);
        SimpleLogger().Write() << "Building package " << _packageId;
        
        _nodeBlockMap.reserve(_nodeIds.size());

        buildHeader();
        buildGraph();
        buildRTree();
        buildNames();
        buildGeometry();
        
        // NOTE: can not clear _nodeBlockMap nor _globalNodeBlockMap, these are needed at later stage
        _nameBlockMap.clear();
        _geometryBlockMap.clear();

        TIMER_STOP(build);
        SimpleLogger().Write() << "Building " << _packageId << " finished after " << TIMER_SEC(build) << " seconds";
    }

    void RoutingGraphBuilder::link(const RoutingGraphBuilder& builder) {
        for (NodeId nodeId : _globalNodeIds) {
            auto it = builder._nodeBlockMap.find(nodeId);
            if (it != builder._nodeBlockMap.end()) {
                GlobalNode globalNode;
                globalNode.package = builder._packageId;
                globalNode.blockIndex = it->second.first;
                globalNode.nodeIndex = it->second.second;
                _linkedGlobalNodeMap[nodeId].push_back(globalNode);
            }
        }
    }
    
    void RoutingGraphBuilder::save() {
        buildGlobalNodes();
        
        auto chunks = std::make_shared<eiff::form_chunk>();
        for (auto it = _fileChunkMap.begin(); it != _fileChunkMap.end(); it++) {
            auto file = std::make_shared<std::fstream>();
            file->exceptions(std::ifstream::failbit | std::ifstream::badbit);
            file->open(it->first, std::ios::binary | std::ios::in);
            auto chunk = std::make_shared<eiff::file_data_chunk>(it->second, file);
            chunks->insert(chunk);
        }

        std::string fileName = _fileName + ".nutigraph";
        SimpleLogger().Write() << "Writing output file " << fileName << "...";
        
        auto file = std::make_shared<std::fstream>();
        file->exceptions(std::ifstream::failbit | std::ifstream::badbit);
        file->open(fileName, std::ios::binary | std::ios::out | std::ios::trunc);
        eiff::write_chunk(*file, chunks);
        chunks.reset();
        
        SimpleLogger().Write() << "Output file " << fileName << " size " << file->tellp() << std::endl;

        for (auto it = _fileChunkMap.begin(); it != _fileChunkMap.end(); it = _fileChunkMap.erase(it)) {
            SimpleLogger().Write() << "Removing temporary file " << it->first << "...";
            unlink(it->first.c_str());
        }
    }

    void RoutingGraphBuilder::buildHeader() {
        bitstreams::output_bitstream bs;
        bs.write_bits(VERSION, 32); // version
        bs.write_bits(_packageId.size(), 16); // package id
        for (char c : _packageId) {
            bs.write_bits(c, 8);
        }

        WGSBounds bbox;
        for (const NodeId& nodeId : _nodeIds) {
            std::vector<WGSPos> geometry = getNodeGeometry(nodeId);
            bbox.add(geometry.begin(), geometry.end());
        }
        cglib::bounding_box<int, 2> bounds = calculateBounds(bbox);
        bs.write_bits(bounds.min(0), 32); // bbox
        bs.write_bits(bounds.min(1), 32);
        bs.write_bits(bounds.max(0), 32);
        bs.write_bits(bounds.max(1), 32);
        
        std::string fileName = _fileName + ".head.tmp";
        auto file = std::make_shared<std::fstream>();
        file->exceptions(std::ifstream::failbit | std::ifstream::badbit);
        file->open(fileName, std::ios::binary | std::ios::out | std::ios::trunc);
        std::vector<unsigned char> data = bs.data();
        file->write(reinterpret_cast<const char*>(data.data()), data.size());
        
        _fileChunkMap[fileName] = eiff::chunk::tag_type{ { 'H', 'E', 'A', 'D' } };
    }
    
    RoutingGraphBuilder::BlockIndex RoutingGraphBuilder::allocateNameId(NameId nameId) {
        auto it = _nameBlockMap.find(nameId);
        if (it != _nameBlockMap.end()) {
            return it->second;
        }
        _nameBlockMap[nameId] = std::make_pair(_encodedNameBlocks.size(), _currentNameBlock.names.size());
        
        std::string name = _graph.getName(nameId);
        _currentNameBlock.names.push_back(name);
        
        std::size_t blockSize = 0;
        if (_options.maxNameBlockSize > 0) {
            bitstreams::output_bitstream bs;
            encodeNameBlock(_currentNameBlock, bs);
            blockSize = bs.data().size();
        }
        
        if (blockSize > _options.maxNameBlockSize || _currentNameBlock.names.size() >= _options.maxNamesPerBlock) {
            writeNameBlock();
        }
        return _nameBlockMap[nameId];
    }
    
    void RoutingGraphBuilder::encodeNameBlock(const Nuti::Routing::RoutingGraphBuilder::NameBlock &block, bitstreams::output_bitstream &bs) const {
        bitstreams::output_bitstream bsTemp = bs;

        int maxLengthBits = 0;
        for (int pass = 0; pass < 2; pass++) {
            bs = bsTemp;

            bs.write_bits(maxLengthBits, 6);
            
            bs.write_bits(block.names.size(), 32);
            for (const std::string& name : block.names) {
                bs.write_bits_variable(name.size(), maxLengthBits);
                for (char c : name) {
                    bs.write_bits(c, 8);
                }
            }
        }
    }

    void RoutingGraphBuilder::writeNameBlock() {
        if (_currentNameBlock.names.empty()) {
            return;
        }

        bitstreams::output_bitstream bs;
        encodeNameBlock(_currentNameBlock, bs);

        _encodedNameBlocks.push_back(bs);
        
        _currentNameBlock = NameBlock();
    }

    void RoutingGraphBuilder::buildNames() {
        writeNameBlock();

        std::string fileName = _fileName + ".names.tmp";
        auto file = writeBlockedFile(fileName, _encodedNameBlocks);
        _fileChunkMap[fileName] = eiff::chunk::tag_type{ { 'N', 'A', 'M', 'E' } };

        SimpleLogger().Write() << "Wrote " << _encodedNameBlocks.size() << " name blocks for " << _packageId << std::endl;

        _encodedNameBlocks.clear();
    }

    RoutingGraphBuilder::BlockIndex RoutingGraphBuilder::allocateGeometryId(PointId startPointId, GeometryId geometryId) {
        auto it = _geometryBlockMap.find(std::make_pair(startPointId, geometryId));
        if (it != _geometryBlockMap.end()) {
            return it->second;
        }
        _geometryBlockMap[std::make_pair(startPointId, geometryId)] = std::make_pair(_encodedGeometryBlocks.size(), _currentGeometryBlock.geometries.size());
        
        std::vector<PointId> geometry;
        geometry.push_back(startPointId);
        for (PointId pointId : _graph.getGeometry(geometryId)) {
            geometry.push_back(pointId);
        }
        
        _currentGeometryBlock.geometries.push_back(geometry);
        for (const PointId& pointId : geometry) {
            Point point = _graph.getPoint(pointId);
            _currentGeometryBlock.minLat = std::min(_currentGeometryBlock.minLat, point.lat);
            _currentGeometryBlock.minLon = std::min(_currentGeometryBlock.minLon, point.lon);
            _currentGeometryBlock.maxLat = std::max(_currentGeometryBlock.maxLat, point.lat);
            _currentGeometryBlock.maxLon = std::max(_currentGeometryBlock.maxLon, point.lon);
        }
        
        std::size_t blockSize = 0;
        if (_options.maxGeometryBlockSize > 0) {
            bitstreams::output_bitstream bs;
            encodeGeometryBlock(_currentGeometryBlock, bs);
            blockSize = bs.data().size();
        }

        if (blockSize > _options.maxGeometryBlockSize || _currentGeometryBlock.geometries.size() >= _options.maxGeometriesPerBlock) {
            writeGeometryBlock();
        }
        return _geometryBlockMap[std::make_pair(startPointId, geometryId)];
    }
    
    void RoutingGraphBuilder::encodeGeometryBlock(const GeometryBlock& block, bitstreams::output_bitstream& bs) const {
        bitstreams::output_bitstream bsTemp1 = bs;

        int maxLatDiffBits = 0;
        int maxLonDiffBits = 0;
        int maxGeometrySizeBits = 0;
        for (int pass = 0; pass < 2; pass++) {
            bs = bsTemp1;

            bs.write_bits(maxLatDiffBits, 6);
            bs.write_bits(maxLonDiffBits, 6);
            bs.write_bits(maxGeometrySizeBits, 6);
            bs.write_bits(block.minLat, 32);
            bs.write_bits(block.minLon, 32);

            bs.write_bits(block.geometries.size(), 32);
            for (const std::vector<PointId>& geometry : block.geometries) {
                bitstreams::output_bitstream bsTemp2 = bs;

                int maxLatZigZagBits = 0;
                int maxLonZigZagBits = 0;
                for (int subPass = 0; subPass < 2; subPass++) {
                    bs = bsTemp2;

                    Point firstPoint = _graph.getPoint(geometry.front());
                    bs.write_bits(maxLatZigZagBits, 6);
                    bs.write_bits(maxLonZigZagBits, 6);
                    bs.write_bits_variable(firstPoint.lat - block.minLat, maxLatDiffBits);
                    bs.write_bits_variable(firstPoint.lon - block.minLon, maxLonDiffBits);

                    bs.write_bits_variable(geometry.size() - 1, maxGeometrySizeBits);
                    for (std::size_t i = 1; i < geometry.size(); i++) {
                        Point point = _graph.getPoint(geometry[i]);
                        Point prevPoint = _graph.getPoint(geometry[i - 1]);
                        int dlat = point.lat - prevPoint.lat;
                        int dlon = point.lon - prevPoint.lon;
                        bs.write_bits_variable(encodeZigZagValue(dlat), maxLatZigZagBits);
                        bs.write_bits_variable(encodeZigZagValue(dlon), maxLonZigZagBits);
                    }
                }
            }
        }
    }

    void RoutingGraphBuilder::writeGeometryBlock() {
        if (_currentGeometryBlock.geometries.empty()) {
            return;
        }

        bitstreams::output_bitstream bs;
        encodeGeometryBlock(_currentGeometryBlock, bs);

        _encodedGeometryBlocks.push_back(bs);

        _currentGeometryBlock = GeometryBlock();
    }

    void RoutingGraphBuilder::buildGeometry() {
        writeGeometryBlock();

        std::string fileName = _fileName + ".geometry.tmp";
        auto file = writeBlockedFile(fileName, _encodedGeometryBlocks);
        _fileChunkMap[fileName] = eiff::chunk::tag_type{ { 'G', 'E', 'O', 'M' } };

        SimpleLogger().Write() << "Wrote " << _encodedGeometryBlocks.size() << " geometry blocks for " << _packageId << std::endl;

        _encodedGeometryBlocks.clear();
    }

    void RoutingGraphBuilder::encodeGlobalNodeBlock(const GlobalNodeBlock& block, bitstreams::output_bitstream& bs) {
        bitstreams::output_bitstream bsTemp = bs;

        int maxPackageNameBits = 0;
        int maxPackagesPerNodeBits = 0;
        int maxGlobalNodeBlockBits = 0;
        int maxGlobalNodeIndexBits = 0;
        for (int pass = 0; pass < 2; pass++) {
            bs = bsTemp;
            bs.write_bits(maxPackageNameBits, 6);
            bs.write_bits(maxPackagesPerNodeBits, 6);
            bs.write_bits(maxGlobalNodeBlockBits, 6);
            bs.write_bits(maxGlobalNodeIndexBits, 6);
            
            bs.write_bits(_currentGlobalNodeBlock.packages.size(), 32);
            for (const std::string& packageId : _currentGlobalNodeBlock.packages) {
                bs.write_bits_variable(packageId.size(), maxPackageNameBits);
                for (char c : packageId) {
                    bs.write_bits(c, 8);
                }
            }
            
            bs.write_bits(_currentGlobalNodeBlock.globalNodes.size(), 32);
            for (const std::pair<NodeId, std::vector<GlobalNode>>& globalNodeInfo : _currentGlobalNodeBlock.globalNodes) {
                bs.write_bits_variable(globalNodeInfo.second.size(), maxPackagesPerNodeBits);
                for (const GlobalNode& globalNode : globalNodeInfo.second) {
                    auto packageIt = std::find(_currentGlobalNodeBlock.packages.begin(), _currentGlobalNodeBlock.packages.end(), globalNode.package);
                    assert(packageIt != _currentGlobalNodeBlock.packages.end());
                    bs.write_bits_variable(packageIt - _currentGlobalNodeBlock.packages.begin(), maxPackagesPerNodeBits);
                    bs.write_bits_variable(globalNode.blockIndex, maxGlobalNodeBlockBits);
                    bs.write_bits_variable(globalNode.nodeIndex, maxGlobalNodeIndexBits);
                }
            }
        }
    }
    
    void RoutingGraphBuilder::writeGlobalNodeBlock() {
        if (_currentGlobalNodeBlock.globalNodes.empty()) {
            return;
        }
        
        bitstreams::output_bitstream bs;
        encodeGlobalNodeBlock(_currentGlobalNodeBlock, bs);

        _encodedGlobalNodeBlocks.push_back(bs);
        
        _currentGlobalNodeBlock = GlobalNodeBlock();
    }
    
    void RoutingGraphBuilder::buildGlobalNodes() {
        TIMER_START(buildGlobalNodeLinks);
        SimpleLogger().Write() << "Building global node links for package " << _packageId << "...";
        
        BlockId lastBlockId = 0;
        for (NodeId globalNodeId : _globalNodeIds) {
            auto blockIt = _globalNodeBlockMap.find(globalNodeId);
            assert(blockIt != _globalNodeBlockMap.end());
            if (blockIt->second.first != lastBlockId) {
                lastBlockId = blockIt->second.first;
                writeGlobalNodeBlock();
            }

            std::vector<GlobalNode> globalNodes;
            auto it = _linkedGlobalNodeMap.find(globalNodeId);
            if (it != _linkedGlobalNodeMap.end()) {
                globalNodes = it->second;
            }
            
            _currentGlobalNodeBlock.globalNodes.emplace_back(globalNodeId, globalNodes);
            for (const GlobalNode& globalNode : globalNodes) {
                if (std::find(_currentGlobalNodeBlock.packages.begin(), _currentGlobalNodeBlock.packages.end(), globalNode.package) == _currentGlobalNodeBlock.packages.end()) {
                    _currentGlobalNodeBlock.packages.push_back(globalNode.package);
                }
            }
        }
        writeGlobalNodeBlock();
        
        std::string fileName = _fileName + ".link.tmp";
        auto file = writeBlockedFile(fileName, _encodedGlobalNodeBlocks);
        _fileChunkMap[fileName] = eiff::chunk::tag_type{ { 'L', 'I', 'N', 'K' } };
        
        TIMER_STOP(buildGlobalNodeLinks);
        SimpleLogger().Write() << "Building global node links for package " << _packageId << " finished after " << TIMER_SEC(buildGlobalNodeLinks) << " seconds";
        SimpleLogger().Write() << "Wrote " << _encodedGlobalNodeBlocks.size() << " global node blocks for package " << _packageId << std::endl;
        
        _encodedGlobalNodeBlocks.clear();
    }
    
    RoutingGraphBuilder::BlockIndex RoutingGraphBuilder::allocateGlobalNodeId(NodeId globalNodeId) {
        auto it = _globalNodeBlockMap.find(globalNodeId);
        if (it != _globalNodeBlockMap.end()) {
            return it->second;
        }
        _globalNodeBlockMap[globalNodeId] = std::make_pair(_globalNodeIds.size() / _options.globalNodesPerBlock, _globalNodeIds.size() % _options.globalNodesPerBlock);
        _globalNodeIds.push_back(globalNodeId);
        return _globalNodeBlockMap[globalNodeId];
    }
    
    void RoutingGraphBuilder::addNodeId(NodeId nodeId) {
        const Node& node = _graph.getNode(nodeId);
        
        _currentNodeBlock.nodes.push_back(nodeId);
        _currentNodeBlock.weightBitsHistogram[bitstreams::get_required_bits(node.nodeData.weight)]++;
        for (EdgeIterator ei = node.firstEdge; ei < node.lastEdge; ei++) {
            const Edge& edge = _graph.getEdge(ei);
            _currentNodeBlock.weightBitsHistogram[bitstreams::get_required_bits(edge.weight)]++;
        }
        BlockIndex geometryBlockIndex = allocateGeometryId(node.nodeData.startPointId, node.nodeData.packedGeometryId);
        BlockIndex nameBlockIndex = allocateNameId(node.nodeData.nameId);
        _currentNodeBlock.minGeometryBlockId = std::min(_currentNodeBlock.minGeometryBlockId, geometryBlockIndex.first);
        _currentNodeBlock.minNameBlockId = std::min(_currentNodeBlock.minNameBlockId, nameBlockIndex.first);
    }
    
    void RoutingGraphBuilder::encodeNodeBlock(const NodeBlock& block, BlockId blockId, bitstreams::output_bitstream& bs) {
        // Calculate optimal weight partioning into 2 sets (small/large weights)
        int largeWeightBits = 0;
        for (unsigned int i = 0; i < block.weightBitsHistogram.size(); i++) {
            if (block.weightBitsHistogram[i] > 0) {
                largeWeightBits = i;
            }
        }
        
        int smallWeightBits = 0;
        std::size_t bestTotalSize = std::numeric_limits<std::size_t>::max();
        for (unsigned int i = 0; i < block.weightBitsHistogram.size(); i++) {
            std::size_t totalSize = 0;
            for (std::size_t j = 0; j < block.weightBitsHistogram.size(); j++) {
                totalSize += (j < i ? i : largeWeightBits) * block.weightBitsHistogram[j];
            }
            if (totalSize < bestTotalSize) {
                bestTotalSize = totalSize;
                smallWeightBits = i;
            }
        }
        
        bitstreams::output_bitstream bsTemp = bs;
        
        int maxInternalNodeIndexBits = 0;
        int maxExternalNodeIndexBits = 0;
        int maxExternalNodeBlockBits = 0;
        int maxGlobalNodeBlockBits = 0;
        int maxGlobalNodeIndexBits = 0;
        int maxContractedNodeBlockBits = 0;
        int maxContractedNodeIndexBits = 0;
        int maxGeometryBlockBits = 0;
        int maxGeometryBlockDiffBits = 0;
        int maxGeometryIndexBits = 0;
        int maxNameBlockBits = 0;
        int maxNameBlockDiffBits = 0;
        int maxNameIndexBits = 0;
        int maxNodeOutDegreeBits = 0;
        int maxTravelModeBits = 0;
        int maxInstructionBits = 0;
        for (int pass = 0; pass < 2; pass++) {
            bs = bsTemp;
            
            bs.write_bits(maxInternalNodeIndexBits, 6);
            bs.write_bits(maxExternalNodeBlockBits, 6);
            bs.write_bits(maxExternalNodeIndexBits, 6);
            bs.write_bits(maxGlobalNodeBlockBits, 6);
            bs.write_bits(maxGlobalNodeIndexBits, 6);
            bs.write_bits(maxContractedNodeBlockBits, 6);
            bs.write_bits(maxContractedNodeIndexBits, 6);
            bs.write_bits(maxGeometryBlockBits, 6);
            bs.write_bits(maxGeometryBlockDiffBits, 6);
            bs.write_bits(maxGeometryIndexBits, 6);
            bs.write_bits(maxNameBlockBits, 6);
            bs.write_bits(maxNameBlockDiffBits, 6);
            bs.write_bits(maxNameIndexBits, 6);
            bs.write_bits(maxNodeOutDegreeBits, 6);
            bs.write_bits(maxTravelModeBits, 6);
            bs.write_bits(maxInstructionBits, 6);
            bs.write_bits(smallWeightBits, 6);
            bs.write_bits(largeWeightBits, 6);
            bs.write_bits_variable(block.minGeometryBlockId, maxGeometryBlockBits);
            bs.write_bits_variable(block.minNameBlockId, maxNameBlockBits);
            
            // Store nodes and outgoing edges
            bs.write_bits(block.nodes.size(), 32);
            for (auto nodeIt = block.nodes.begin(); nodeIt != block.nodes.end(); nodeIt++) {
                NodeId nodeId = *nodeIt;
                const Node& node = _graph.getNode(nodeId);
                BlockIndex geometryBlockIndex = allocateGeometryId(node.nodeData.startPointId, node.nodeData.packedGeometryId);
                BlockIndex nameBlockIndex = allocateNameId(node.nodeData.nameId);
                
                bs.write_bits_variable(node.lastEdge - node.firstEdge, maxNodeOutDegreeBits);
                bs.write_bits_variable(geometryBlockIndex.first - block.minGeometryBlockId, maxGeometryBlockDiffBits);
                bs.write_bits_variable(geometryBlockIndex.second, maxGeometryIndexBits);
                bs.write_bit(node.nodeData.geometryReversed);
                bs.write_bits_variable(nameBlockIndex.first - block.minNameBlockId, maxNameBlockDiffBits);
                bs.write_bits_variable(nameBlockIndex.second, maxNameIndexBits);
                bs.write_bits_variable(encodeTravelMode(node.nodeData.travelMode), maxTravelModeBits);
                if (node.nodeData.weight >= (1U << smallWeightBits)) {
                    bs.write_bit(true);
                    bs.write_bits(node.nodeData.weight, largeWeightBits);
                }
                else {
                    bs.write_bit(false);
                    bs.write_bits(node.nodeData.weight, smallWeightBits);
                }
                
                for (EdgeIterator ei = node.firstEdge; ei < node.lastEdge; ei++) {
                    const Edge& edge = _graph.getEdge(ei);
                    if (!std::binary_search(_nodeIds.begin(), _nodeIds.end(), edge.targetNodeId)) {
                        BlockIndex globalBlockIndex = allocateGlobalNodeId(edge.targetNodeId);
                        bs.write_bit(false);
                        bs.write_bits_variable(0, maxInternalNodeIndexBits);
                        bs.write_bits_variable(globalBlockIndex.first, maxGlobalNodeBlockBits);
                        bs.write_bits_variable(globalBlockIndex.second, maxGlobalNodeIndexBits);
                    }
                    else {
                        auto targetNodeIt = std::find(block.nodes.begin(), nodeIt, edge.targetNodeId);
                        if (targetNodeIt == nodeIt) {
                            auto nodeBlockIt = _nodeBlockMap.find(edge.targetNodeId);
                            assert(nodeBlockIt != _nodeBlockMap.end());
                            int delta = static_cast<int>(blockId) - static_cast<int>(nodeBlockIt->second.first);
                            assert(delta >= 0);
                            bs.write_bit(true);
                            bs.write_bits_variable(delta, maxExternalNodeBlockBits);
                            bs.write_bits_variable(nodeBlockIt->second.second, maxExternalNodeIndexBits);
                        }
                        else {
                            std::size_t delta = nodeIt - targetNodeIt;
                            bs.write_bit(false);
                            bs.write_bits_variable(delta, maxInternalNodeIndexBits);
                        }
                    }
                    bs.write_bit(edge.forward);
                    bs.write_bit(edge.backward);
                    if (edge.weight >= (1U << smallWeightBits)) {
                        bs.write_bit(true);
                        bs.write_bits(edge.weight, largeWeightBits);
                    }
                    else {
                        bs.write_bit(false);
                        bs.write_bits(edge.weight, smallWeightBits);
                    }
                    if (edge.contractedNodeId != -1) {
                        bs.write_bit(true);
                        if (!std::binary_search(_nodeIds.begin(), _nodeIds.end(), edge.contractedNodeId)) {
                            BlockIndex globalBlockIndex = allocateGlobalNodeId(edge.contractedNodeId);
                            bs.write_bit(false);
                            bs.write_bits_variable(0, maxInternalNodeIndexBits);
                            bs.write_bits_variable(globalBlockIndex.first, maxGlobalNodeBlockBits);
                            bs.write_bits_variable(globalBlockIndex.second, maxGlobalNodeIndexBits);
                        }
                        else {
                            auto contractedNodeIt = std::find(block.nodes.begin(), nodeIt, edge.contractedNodeId);
                            if (contractedNodeIt == nodeIt) {
                                std::size_t contractedBlockIndex = blockId;
                                std::size_t contractedNodeIndex = 255;
                                auto nodeBlockIt = _nodeBlockMap.find(edge.contractedNodeId);
                                if (nodeBlockIt != _nodeBlockMap.end()) { // NOTE: if not found, then this is not an error, we will have correct value in the second pass
                                    contractedBlockIndex = nodeBlockIt->second.first;
                                    contractedNodeIndex = nodeBlockIt->second.second;
                                }
                                int delta = static_cast<int>(contractedBlockIndex) - static_cast<int>(blockId);
                                bs.write_bit(true);
                                bs.write_bits_variable(encodeZigZagValue(delta), maxContractedNodeBlockBits);
                                bs.write_bits_variable(contractedNodeIndex, maxContractedNodeIndexBits);
                            }
                            else {
                                std::size_t delta = nodeIt - contractedNodeIt;
                                bs.write_bit(false);
                                bs.write_bits_variable(delta, maxInternalNodeIndexBits);
                            }
                        }
                    }
                    else {
                        bs.write_bit(false);
                        bs.write_bits_variable(edge.edgeData.turnInstruction, maxInstructionBits);
                    }
                }
            }
        }
    }
    
    void RoutingGraphBuilder::writeNodeBlock() {
        if (_currentNodeBlock.nodes.empty()) {
            return;
        }
        
        bitstreams::output_bitstream bs;
        encodeNodeBlock(_currentNodeBlock, _encodedNodeBlocks.size(), bs);
        
        _encodedNodeBlocks.push_back(bs);
        
        _currentNodeBlock = NodeBlock();
    }
    
    void RoutingGraphBuilder::buildGraph() {
        TIMER_START(buildGraph);
        SimpleLogger().Write() << "Building graph for package " << _packageId << "...";
        
        std::vector<NodeId> nodeIds = getTopologicalSpatialNodeOrdering();
        
        // First pass, do approximate division into blocks (we do not know how to encode contracted nodes yet)
        for (NodeId nodeId : nodeIds) {
            _nodeBlockMap[nodeId] = std::make_pair(_encodedNodeBlocks.size(), _currentNodeBlock.nodes.size());
            
            addNodeId(nodeId);
            
            std::size_t blockSize = 0;
            if (_options.maxNodeBlockSize > 0) {
                bitstreams::output_bitstream bs;
                encodeNodeBlock(_currentNodeBlock, _encodedNodeBlocks.size(), bs);
                blockSize = bs.data().size();
            }

            if (blockSize > _options.maxNodeBlockSize || _currentNodeBlock.nodes.size() >= _options.maxNodesPerBlock) {
                writeNodeBlock();
            }
        }
        writeNodeBlock();
        
        _encodedNodeBlocks.clear();
        
        // Second pass, do final encoding. We keep blocks from the first pass
        BlockId lastBlockId = 0;
        for (NodeId nodeId : nodeIds) {
            auto blockIt = _nodeBlockMap.find(nodeId);
            assert(blockIt != _nodeBlockMap.end());
            
            if (blockIt->second.first != lastBlockId) {
                lastBlockId = blockIt->second.first;
                writeNodeBlock();
            }
            
            addNodeId(nodeId);
        }
        writeNodeBlock();
        
        std::string fileName = _fileName + ".hsgr.tmp";
        auto file = writeBlockedFile(fileName, _encodedNodeBlocks);
        _fileChunkMap[fileName] = eiff::chunk::tag_type{ { 'N', 'O', 'D', 'E' } };
        
        TIMER_STOP(buildGraph);
        SimpleLogger().Write() << "Building graph for package " << _packageId << " finished after " << TIMER_SEC(buildGraph) << " seconds";
        
        SimpleLogger().Write() << "Wrote " << _encodedNodeBlocks.size() << " node blocks for package " << _packageId << std::endl;
        
        _encodedNodeBlocks.clear();
    }
    
    void RoutingGraphBuilder::addRTreeNode(const RTreeNode& node) {
        _currentRTreeNodeBlock.nodes.push_back(node);
        for (auto it = node.children.begin(); it != node.children.end(); it++) {
            cglib::bounding_box<int, 2> bounds = calculateBounds(it->first);
            _currentRTreeNodeBlock.minLat = std::min(_currentRTreeNodeBlock.minLat, bounds.min(0));
            _currentRTreeNodeBlock.minLon = std::min(_currentRTreeNodeBlock.minLon, bounds.min(1));
            _currentRTreeNodeBlock.maxLat = std::max(_currentRTreeNodeBlock.maxLat, bounds.max(0));
            _currentRTreeNodeBlock.maxLon = std::max(_currentRTreeNodeBlock.maxLon, bounds.max(1));
        }
        for (auto it = node.nodeBlocks.begin(); it != node.nodeBlocks.end(); it++) {
            cglib::bounding_box<int, 2> bounds = calculateBounds(it->first);
            _currentRTreeNodeBlock.minLat = std::min(_currentRTreeNodeBlock.minLat, bounds.min(0));
            _currentRTreeNodeBlock.minLon = std::min(_currentRTreeNodeBlock.minLon, bounds.min(1));
            _currentRTreeNodeBlock.maxLat = std::max(_currentRTreeNodeBlock.maxLat, bounds.max(0));
            _currentRTreeNodeBlock.maxLon = std::max(_currentRTreeNodeBlock.maxLon, bounds.max(1));
        }
    }
    
    void RoutingGraphBuilder::encodeRTreeNodeBlock(const RTreeNodeBlock& block, BlockId blockId, bitstreams::output_bitstream& bs) {
        bitstreams::output_bitstream bsTemp = bs;

        int maxRTreeBlockBits = 0;
        int maxRTreeIndexBits = 0;
        int maxNodeBlockBits = 0;
        int maxSizeBits = 0;
        int maxLatDiffBits = 0;
        int maxLonDiffBits = 0;
        int maxLatDiffBits2 = 0;
        int maxLonDiffBits2 = 0;
        for (int pass = 0; pass < 2; pass++) {
            bs = bsTemp;
            bs.write_bits(maxRTreeBlockBits, 6);
            bs.write_bits(maxRTreeIndexBits, 6);
            bs.write_bits(maxNodeBlockBits, 6);
            bs.write_bits(maxSizeBits, 6);
            bs.write_bits(maxLatDiffBits, 6);
            bs.write_bits(maxLonDiffBits, 6);
            bs.write_bits(maxLatDiffBits2, 6);
            bs.write_bits(maxLonDiffBits2, 6);
            
            bs.write_bits(block.minLat, 32);
            bs.write_bits(block.minLon, 32);
            
            bs.write_bits(block.nodes.size(), 32);
            for (const RTreeNode& node : block.nodes) {
                bs.write_bit(node.leaf);
                if (node.leaf) {
                    bs.write_bits_variable(node.nodeBlocks.size(), maxSizeBits);
                    for (auto it = node.nodeBlocks.begin(); it != node.nodeBlocks.end(); it++) {
                        cglib::bounding_box<int, 2> bounds = calculateBounds(it->first);
                        bs.write_bits_variable(bounds.min(0) - block.minLat, maxLatDiffBits);
                        bs.write_bits_variable(bounds.min(1) - block.minLon, maxLonDiffBits);
                        bs.write_bits_variable(bounds.max(0) - bounds.min(0), maxLatDiffBits2);
                        bs.write_bits_variable(bounds.max(1) - bounds.min(1), maxLonDiffBits2);
                        bs.write_bits_variable(it->second, maxNodeBlockBits);
                    }
                }
                else {
                    bs.write_bits_variable(node.children.size(), maxSizeBits);
                    for (auto it = node.children.begin(); it != node.children.end(); it++) {
                        cglib::bounding_box<int, 2> bounds = calculateBounds(it->first);
                        bs.write_bits_variable(bounds.min(0) - block.minLat, maxLatDiffBits);
                        bs.write_bits_variable(bounds.min(1) - block.minLon, maxLonDiffBits);
                        bs.write_bits_variable(bounds.max(0) - bounds.min(0), maxLatDiffBits2);
                        bs.write_bits_variable(bounds.max(1) - bounds.min(1), maxLonDiffBits2);
                        
                        BlockIndex blockIndex(it->second / _options.rtreeNodesPerBlock, it->second % _options.rtreeNodesPerBlock);
                        int delta = static_cast<int>(blockIndex.first) - static_cast<int>(blockId);
                        assert(delta >= 0);
                        bs.write_bits_variable(delta, maxRTreeBlockBits);
                        bs.write_bits_variable(blockIndex.second, maxRTreeIndexBits);
                    }
                }
            }
        }
    }
    
    void RoutingGraphBuilder::writeRTreeNodeBlock() {
        if (_currentRTreeNodeBlock.nodes.empty()) {
            return;
        }
        
        bitstreams::output_bitstream bs;
        encodeRTreeNodeBlock(_currentRTreeNodeBlock, _encodedRTreeNodeBlocks.size(), bs);
        
        _encodedRTreeNodeBlocks.push_back(bs);
        
        _currentRTreeNodeBlock = RTreeNodeBlock();
    }
    
    void RoutingGraphBuilder::buildRTree() {
        namespace bg = boost::geometry;
        namespace bgi = boost::geometry::index;

        typedef bg::model::point<double, 2, bg::cs::cartesian> Point;
        typedef bg::model::box<Point> Box;
        typedef std::pair<Box, BlockId> Value;

        TIMER_START(buildRTree);
        SimpleLogger().Write() << "Building RTree...";

        // Calculate node block bounds
        std::map<BlockId, cglib::bounding_box<double, 2>> blockBoundsMap;
        for (const NodeId& nodeId : _nodeIds) {
            std::vector<WGSPos> geometry = getNodeGeometry(nodeId);
            cglib::bounding_box<double, 2> bbox;
            bbox.add(geometry.begin(), geometry.end());
            auto it = _nodeBlockMap.find(nodeId);
            assert(it != _nodeBlockMap.end());
            auto blockMapIt = blockBoundsMap.find(it->second.first);
            if (blockMapIt == blockBoundsMap.end()) {
                blockBoundsMap[it->second.first] = bbox;
            } else {
                blockMapIt->second.add(bbox);
            }
        }
        
        // Build RTree
        std::vector<Value> values;
        for (auto it = blockBoundsMap.begin(); it != blockBoundsMap.end(); it++) {
            const cglib::bounding_box<double, 2> bbox = it->second;
            values.emplace_back(Box(Point(bbox.min(0), bbox.min(1)), Point(bbox.max(0), bbox.max(1))), it->first);
        }
        bgi::rtree<Value, bgi::linear<16, 4>> rtree(values.begin(), values.end());
        
        // Extract DFS nodes, remap to BFS for better node locality
        auto dfsNodes = bgi::extract_rtree(rtree);
        auto bfsNodes = dfsNodes;
        bfsNodes.clear();
        std::map<std::size_t, std::size_t> bfsRemap;
        std::queue<std::size_t> queue;
        if (!dfsNodes.empty()) {
            queue.push(0);
        }
        while (!queue.empty()) {
            std::size_t index = queue.front();
            queue.pop();
            bfsRemap[index] = bfsNodes.size();
            
            const auto& node = dfsNodes[index];
            bfsNodes.push_back(node);
            if (!node.leaf) {
                for (auto it = node.children.begin(); it != node.children.end(); it++) {
                    queue.push(*it);
                }
            }
        }
        assert(bfsRemap.size() == dfsNodes.size());
        
        // Build blocked RTree
        for (auto it = bfsNodes.begin(); it != bfsNodes.end(); it++) {
            RTreeNode node;
            node.leaf = it->leaf;
            for (std::size_t dfsChildIdx : it->children) {
                std::size_t bfsChildIdx = bfsRemap[dfsChildIdx];
                Point childMin = bfsNodes.at(bfsChildIdx).box.min_corner();
                Point childMax = bfsNodes.at(bfsChildIdx).box.max_corner();
                WGSBounds bbox(WGSPos(childMin.get<0>(), childMin.get<1>()), WGSPos(childMax.get<0>(), childMax.get<1>()));
                node.children.emplace_back(bbox, bfsChildIdx);
            }
            for (auto value : it->values) {
                node.nodeBlocks.emplace_back(blockBoundsMap[value.second], value.second);
            }
            
            addRTreeNode(node);
            
            if (_currentRTreeNodeBlock.nodes.size() >= _options.rtreeNodesPerBlock) {
                writeRTreeNodeBlock();
            }
        }
        writeRTreeNodeBlock();
        
        std::string fileName = _fileName + ".rtree.tmp";
        auto file = writeBlockedFile(fileName, _encodedRTreeNodeBlocks);
        _fileChunkMap[fileName] = eiff::chunk::tag_type{ { 'R', 'T', 'R', 'E' } };
        
        TIMER_STOP(buildRTree);
        SimpleLogger().Write() << "Building finished after " << TIMER_SEC(buildRTree) << " seconds";
        
        SimpleLogger().Write() << "Wrote " << _encodedRTreeNodeBlocks.size() << " blocks" << std::endl;
        
        _encodedRTreeNodeBlocks.clear();
    }
    
    std::vector<RoutingGraphBuilder::WGSPos> RoutingGraphBuilder::getNodeGeometry(NodeId nodeId) const {
        const Node& node = _graph.getNode(nodeId);
        std::vector<WGSPos> geometry;
        geometry.push_back(getWGSPos(_graph.getPoint(node.nodeData.startPointId)));
        for (PointId pointId : _graph.getGeometry(node.nodeData.packedGeometryId)) {
            geometry.push_back(getWGSPos(_graph.getPoint(pointId)));
        }
        if (node.nodeData.geometryReversed) {
            std::reverse(geometry.begin(), geometry.end());
        }
        return geometry;
    }

    std::vector<RoutingGraphBuilder::NodeId> RoutingGraphBuilder::getTopologicalSpatialNodeOrdering() const {
        WGSBounds bbox;
        for (NodeId nodeId : _nodeIds) {
            std::vector<WGSPos> geometry = getNodeGeometry(nodeId);
            bbox.add(geometry.begin(), geometry.end());
        }

        std::vector<std::vector<NodeId>> transposedNodes(_graph.getNodeCount());
        for (NodeId nodeId : _nodeIds) {
            const Node& node = _graph.getNode(nodeId);
            for (EdgeIterator ei = node.firstEdge; ei != node.lastEdge; ei++) {
                const Edge& edge = _graph.getEdge(ei);
                if (std::binary_search(_nodeIds.begin(), _nodeIds.end(), edge.targetNodeId)) {
                    transposedNodes.at(edge.targetNodeId).push_back(nodeId);
                }
            }
        }

        std::priority_queue<std::tuple<long long, long long, NodeId>> pq;
        long long nodePriority = 0;
        for (NodeId nodeId : _nodeIds) {
            const Node& node = _graph.getNode(nodeId);
            std::vector<WGSPos> geometry = getNodeGeometry(nodeId);
            bool topNode = true;
            for (EdgeIterator ei = node.firstEdge; ei != node.lastEdge; ei++) {
                const Edge& edge = _graph.getEdge(ei);
                if (std::binary_search(_nodeIds.begin(), _nodeIds.end(), edge.targetNodeId)) {
                    topNode = false;
                    break;
                }
            }
            if (topNode) {
                const WGSPos& pos = geometry.at(geometry.size() / 2);
                long long code = calculateHilbertCode(pos, bbox);
                pq.emplace(code, nodePriority++, nodeId);
            }
        }

        std::vector<NodeId> nodeOrdering;
        nodeOrdering.reserve(_graph.getNodeCount());
        std::unordered_set<NodeId> processedNodes;
        while (!pq.empty()) {
            NodeId nodeId = std::get<2>(pq.top());
            pq.pop();
            if (processedNodes.count(nodeId) > 0) {
                continue;
            }
            processedNodes.insert(nodeId);

            nodeOrdering.push_back(nodeId);

            for (NodeId targetNodeId : transposedNodes.at(nodeId)) {
                const Node& targetNode = _graph.getNode(targetNodeId);
                bool allOutEdgesProcessed = true;
                for (EdgeIterator ei = targetNode.firstEdge; ei != targetNode.lastEdge; ei++) {
                    const Edge& edge = _graph.getEdge(ei);
                    if (std::binary_search(_nodeIds.begin(), _nodeIds.end(), edge.targetNodeId)) {
                        allOutEdgesProcessed = allOutEdgesProcessed && processedNodes.count(edge.targetNodeId) > 0;
                    }
                }
                if (allOutEdgesProcessed) {
                    std::vector<WGSPos> geometry = getNodeGeometry(targetNodeId);
                    const WGSPos& pos = geometry.at(geometry.size() / 2);
                    long long code = calculateHilbertCode(pos, bbox);
                    pq.emplace(code, nodePriority++, targetNodeId);
                }
            }
        }
        assert(nodeOrdering.size() == _nodeIds.size());
        return nodeOrdering;
    }
    
    std::shared_ptr<std::fstream> RoutingGraphBuilder::writeBlockedFile(const std::string& fileName, const std::vector<bitstreams::output_bitstream>& blocks) {
        auto file = std::make_shared<std::fstream>();
        file->exceptions(std::ifstream::failbit | std::ifstream::badbit);
        file->open(fileName, std::ios::binary | std::ios::out | std::ios::trunc);

        std::uint32_t blockCount = static_cast<std::uint32_t>(blocks.size());
        file->write(reinterpret_cast<const char*>(&blockCount), sizeof(std::uint32_t));
        std::uint64_t blockOffset = sizeof(std::uint32_t) + (blockCount + 1) * sizeof(std::uint64_t);
        for (const bitstreams::output_bitstream& bs : blocks) {
            file->write(reinterpret_cast<const char*>(&blockOffset), sizeof(std::uint64_t));
            blockOffset += bs.data().size();
        }
        file->write(reinterpret_cast<const char*>(&blockOffset), sizeof(std::uint64_t));

        for (const bitstreams::output_bitstream& bs : blocks) {
            std::vector<unsigned char> data = bs.data();
            file->write(reinterpret_cast<const char*>(data.data()), data.size());
        }
        return file;
    }
    
    cglib::bounding_box<int, 2> RoutingGraphBuilder::calculateBounds(const WGSBounds& bbox) {
        cglib::bounding_box<int, 2> bboxInt;
        for (int i = 0; i < 2; i++) {
            bboxInt.min(i) = static_cast<int>(std::floor(bbox.min(i) / COORDINATE_SCALE));
            bboxInt.max(i) = static_cast<int>(std::ceil(bbox.max(i) / COORDINATE_SCALE));
        }
        return bboxInt;
    }

    long long RoutingGraphBuilder::calculateHilbertCode(const WGSPos& pos, const WGSBounds& bbox) {
        int x = static_cast<int>((pos(0) + bbox.min(0)) * (1 << 24) / bbox.size()(0));
        int y = static_cast<int>((pos(1) + bbox.min(1)) * (1 << 24) / bbox.size()(1));
        return hilbertXY2D(1 << 24, x, y);
    }

    RoutingGraphBuilder::WGSPos RoutingGraphBuilder::getWGSPos(const Point& point) {
        return WGSPos(point.lat * COORDINATE_SCALE, point.lon * COORDINATE_SCALE);
    }

    unsigned int RoutingGraphBuilder::encodeZigZagValue(int val) {
        return (val < 0 ? static_cast<unsigned int>(-val) * 2 - 1 : static_cast<unsigned int>(val) * 2);
    }

    unsigned char RoutingGraphBuilder::encodeTravelMode(unsigned char travelMode) {
        if (travelMode == 0 || travelMode == 1) {
            return travelMode ^ 1; // use 0 for DEFAULT and 1 for RESTRICTED, allows more compact reprentation
        }
        return travelMode;
    }

    const int RoutingGraphBuilder::VERSION = 0;

    const double RoutingGraphBuilder::COORDINATE_SCALE = 1.0e-6;
} }
