#include "OSRMGraph.h"

#include "osrm/simple_logger.hpp"
#include "osrm/timing_util.hpp"

#include <fstream>
#include <functional>
#include <numeric>
#include <utility>
#include <tuple>
#include <queue>
#include <stack>
#include <map>
#include <set>

namespace Nuti { namespace Routing {
    OSRMGraph::OSRMGraph(const std::string& fileName) :
        _fileName(fileName)
    {
        importNames();
        importPoints();
        importGeometry();
        importGraph();
    }

    std::size_t OSRMGraph::getNodeCount() const {
        std::lock_guard<std::mutex> lock(_nodesMutex);
        return _nodes.size();
    }

    const OSRMGraph::Node& OSRMGraph::getNode(NodeId nodeId) const {
        std::lock_guard<std::mutex> lock(_nodesMutex);
        return _nodes.at(nodeId);
    }

    const OSRMGraph::Edge& OSRMGraph::getEdge(EdgeIterator edgeId) const {
        std::lock_guard<std::mutex> lock(_edgesMutex);
        return _edges.at(edgeId);
    }

    const std::string& OSRMGraph::getName(NameId nameId) const {
        static const std::string empty;

        std::lock_guard<std::mutex> lock(_namesMutex);
        if (nameId == std::numeric_limits<NameId>::max()) {
            return empty;
        }
        return _names.at(nameId);
    }

    const OSRMGraph::Point& OSRMGraph::getPoint(PointId pointId) const {
        std::lock_guard<std::mutex> lock(_geometryMutex);
        return _geometryPoints.at(pointId);
    }

    std::vector<OSRMGraph::PointId> OSRMGraph::getGeometry(GeometryId geometryId) const {
        std::lock_guard<std::mutex> lock(_geometryMutex);
        std::size_t first_index = _geometryIndices.at(geometryId);
        std::size_t last_index = (geometryId + 1 < _geometryIndices.size() ? _geometryIndices.at(geometryId + 1) : _geometryPointIds.size());
        return std::vector<PointId>(_geometryPointIds.begin() + first_index, _geometryPointIds.begin() + last_index);
    }

    void OSRMGraph::importNames() {
        enum { BLOCK_SIZE = 16 };

        struct Header {
            unsigned int number_of_blocks;
            unsigned int sum_lengths;
        };

        TIMER_START(importNames);
        SimpleLogger().Write() << "Importing names...";

        std::string fileName = _fileName + ".names";
        std::ifstream file;
        file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        file.open(fileName, std::ios::binary);

        Header header;
        file.read(reinterpret_cast<char*>(&header), sizeof(Header));
        std::vector<unsigned int> block_offsets(header.number_of_blocks);
        file.read(reinterpret_cast<char*>(block_offsets.data()), sizeof(unsigned int) * header.number_of_blocks);
        std::vector<std::array<unsigned char, BLOCK_SIZE> > diff_blocks(header.number_of_blocks);
        file.read(reinterpret_cast<char*>(diff_blocks.data()), sizeof(unsigned char) * BLOCK_SIZE * header.number_of_blocks);

        unsigned int number_of_chars = 0;
        file.read(reinterpret_cast<char*>(&number_of_chars), sizeof(unsigned int));
        std::vector<char> data_table(number_of_chars);
        file.read(reinterpret_cast<char*>(data_table.data()), sizeof(char) * data_table.size());

        for (unsigned int block = 0; block < header.number_of_blocks; block++) {
            std::size_t offset = block_offsets[block];
            std::size_t nextOffset = (block + 1 < header.number_of_blocks ? block_offsets[block + 1] : data_table.size());
            for (unsigned char diff : diff_blocks[block]) {
                _names.push_back(std::string(data_table.data() + offset, diff));
                offset += diff;
            }
            _names.push_back(std::string(data_table.data() + offset, nextOffset - offset));
        }

        TIMER_STOP(importNames);
        SimpleLogger().Write() << "Importing finished after " << TIMER_SEC(importNames) << " seconds";

        SimpleLogger().Write() << "Imported " << _names.size() << " names" << std::endl;
    }

    void OSRMGraph::importPoints() {
        struct QueryNode {
            int lat;
            int lon;
            std::uint64_t node_id;
        };

        TIMER_START(importPoints);
        SimpleLogger().Write() << "Importing points...";

        std::string fileName = _fileName + ".nodes";
        std::ifstream file;
        file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        file.open(fileName, std::ios::binary);

        unsigned int number_of_coordinates = 0;
        file.read(reinterpret_cast<char*>(&number_of_coordinates), sizeof(unsigned int));

        _geometryPoints.reserve(number_of_coordinates);
        for (unsigned int i = 0; i < number_of_coordinates; i++) {
            QueryNode query_node;
            file.read(reinterpret_cast<char*>(&query_node), sizeof(QueryNode));

            Point geomPoint;
            geomPoint.lat = query_node.lat;
            geomPoint.lon = query_node.lon;
            _geometryPoints.push_back(geomPoint);
        }

        TIMER_STOP(importPoints);
        SimpleLogger().Write() << "Importing finished after " << TIMER_SEC(importPoints) << " seconds";

        SimpleLogger().Write() << "Imported " << _geometryPoints.size() << " points" << std::endl;
    }

    void OSRMGraph::importGeometry() {
        TIMER_START(importGeometry);
        SimpleLogger().Write() << "Importing geometry...";

        std::string fileName = _fileName + ".geometry";
        std::ifstream file;
        file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        file.open(fileName, std::ios::binary);

        unsigned int number_of_indices = 0;
        file.read(reinterpret_cast<char*>(&number_of_indices), sizeof(unsigned int));
        _geometryIndices.reserve(number_of_indices);
        for (unsigned int i = 0; i < number_of_indices; i++) {
            unsigned int geom_index = std::numeric_limits<unsigned int>::max();
            file.read(reinterpret_cast<char*>(&geom_index), sizeof(unsigned int));
            _geometryIndices.push_back(geom_index);
        }

        unsigned int number_of_compressed_geometries = 0;
        file.read(reinterpret_cast<char*>(&number_of_compressed_geometries), sizeof(unsigned int));
        _geometryPointIds.reserve(number_of_compressed_geometries);
        for (unsigned int i = 0; i < number_of_compressed_geometries; i++) {
            unsigned int point_index = std::numeric_limits<unsigned int>::max();
            file.read(reinterpret_cast<char*>(&point_index), sizeof(unsigned int));
            _geometryPointIds.push_back(point_index);
        }

        TIMER_STOP(importGeometry);
        SimpleLogger().Write() << "Importing finished after " << TIMER_SEC(importGeometry) << " seconds";

        SimpleLogger().Write() << "Imported " << _geometryIndices.size() << " indices, " << _geometryPointIds.size() << " elements" << std::endl;
    }

    void OSRMGraph::importNodes(NodeDataVector& nodeDataMap) {
        struct EdgeBasedNode {
            unsigned int forward_edge_based_node_id; // needed for edge-expanded graph
            unsigned int reverse_edge_based_node_id; // needed for edge-expanded graph
            unsigned int u;                          // indices into the coordinates array
            unsigned int v;                          // indices into the coordinates array
            unsigned int name_id;                  // id of the edge name
            int forward_weight;                // weight of the edge
            int reverse_weight;                // weight in the other direction (may be different)
            int forward_offset;          // prefix sum of the weight up the edge TODO: short must suffice
            int reverse_offset;          // prefix sum of the weight from the edge TODO: short must suffice
            unsigned int packed_geometry_id; // if set, then the edge represents a packed geometry
            struct {
                unsigned id : 31;
                bool is_tiny : 1;
            } component;
            unsigned short fwd_segment_position; // segment id in a compressed geometry
            unsigned char forward_travel_mode : 4;
            unsigned char backward_travel_mode : 4;
        };

        struct LeafNode {
            uint32_t object_count;
            std::array<EdgeBasedNode, 1024> objects;
        };

        TIMER_START(importSpatialNodes);
        SimpleLogger().Write() << "Importing spatial index nodes...";

        std::string fileName = _fileName + ".fileIndex";
        std::ifstream file;
        file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        file.open(fileName, std::ios::binary);

        file.seekg(0, std::ios::end);
        std::streampos file_size = file.tellg();
        file.seekg(0, std::ios::beg);
        unsigned int number_of_nodes = 0;
        file.read(reinterpret_cast<char*>(&number_of_nodes), sizeof(unsigned int));
        file.seekg(4, std::ios::cur);

        std::map<std::tuple<unsigned int, unsigned int>, unsigned int> unpackedGeometryMap;
        for (int block = 0; true; block++) {
            std::streampos file_offset = static_cast<std::streampos>(sizeof(LeafNode)) * block + 8;
            if (file_offset >= file_size) {
                break;
            }
            LeafNode leaf_node;
            file.read(reinterpret_cast<char*>(&leaf_node), sizeof(LeafNode));
            for (unsigned int i = 0; i < leaf_node.object_count; i++) {

                if (leaf_node.objects[i].packed_geometry_id == -1) {
                    std::tuple<unsigned int, unsigned int> geomId(leaf_node.objects[i].v, leaf_node.objects[i].v);
                    auto it = unpackedGeometryMap.find(geomId);
                    if (it != unpackedGeometryMap.end()) {
                        leaf_node.objects[i].packed_geometry_id = it->second;
                    }
                    else {
                        leaf_node.objects[i].packed_geometry_id = static_cast<unsigned int>(_geometryIndices.size());
                        _geometryIndices.push_back(_geometryPointIds.size());
                        _geometryPointIds.push_back(leaf_node.objects[i].v);
                        unpackedGeometryMap[geomId] = leaf_node.objects[i].packed_geometry_id;
                    }
                }

                if (leaf_node.objects[i].forward_edge_based_node_id != -1) {
                    NodeData nodeData;
                    nodeData.nameId = leaf_node.objects[i].name_id;
                    nodeData.startPointId = leaf_node.objects[i].u;
                    nodeData.packedGeometryId = leaf_node.objects[i].packed_geometry_id;
                    nodeData.geometryReversed = false;
                    nodeData.travelMode = leaf_node.objects[i].forward_travel_mode;
                    nodeData.weight = leaf_node.objects[i].forward_offset + leaf_node.objects[i].forward_weight;
                    unsigned int node_id = leaf_node.objects[i].forward_edge_based_node_id;
                    if (nodeDataMap.size() <= node_id) {
                        nodeDataMap.resize(node_id + 1);
                    }
                    NodeData& nodeDataOld = nodeDataMap.at(node_id);
                    if (nodeDataOld.packedGeometryId == -1) {
                        nodeDataOld = nodeData;
                    }
                    else {
                        if (nodeData.nameId != nodeDataOld.nameId || nodeData.packedGeometryId != nodeDataOld.packedGeometryId || nodeData.travelMode != nodeDataOld.travelMode) {
                            SimpleLogger().Write(logWARNING) << "Node " << node_id << " data mismatch!";
                            if (nodeData.packedGeometryId != nodeDataOld.packedGeometryId) {
                                std::vector<PointId> geom = getGeometry(nodeData.packedGeometryId);
                                std::vector<PointId> geomOld = getGeometry(nodeDataOld.packedGeometryId);
                                if (geom.size() > geomOld.size()) {
                                    SimpleLogger().Write() << "Replacing geometry...";
                                    nodeDataOld = nodeData;
                                }
                            }
                        }
                        nodeDataOld.weight = std::max(nodeData.weight, nodeDataOld.weight);

                        unsigned int geom_index = nodeData.packedGeometryId;
                        const auto& geometryIndices = _geometryIndices;
                        std::size_t first_index = geometryIndices.at(geom_index);
                        std::size_t last_index = (geom_index + 1 < geometryIndices.size() ? geometryIndices.at(geom_index + 1) : _geometryPointIds.size());
                        bool u_found = std::find(_geometryPointIds.begin() + first_index, _geometryPointIds.begin() + last_index, nodeData.startPointId) != _geometryPointIds.begin() + last_index;
                        if (!u_found) {
                            nodeDataOld.startPointId = nodeData.startPointId;
                        }
                    }
                }

                if (leaf_node.objects[i].reverse_edge_based_node_id != -1) {
                    NodeData nodeData;
                    nodeData.nameId = leaf_node.objects[i].name_id;
                    nodeData.startPointId = leaf_node.objects[i].u;
                    nodeData.packedGeometryId = leaf_node.objects[i].packed_geometry_id;
                    nodeData.geometryReversed = true;

                    nodeData.travelMode = leaf_node.objects[i].backward_travel_mode;
                    nodeData.weight = leaf_node.objects[i].reverse_offset + leaf_node.objects[i].reverse_weight;
                    unsigned int node_id = leaf_node.objects[i].reverse_edge_based_node_id;
                    if (nodeDataMap.size() <= node_id) {
                        nodeDataMap.resize(node_id + 1);
                    }
                    NodeData& nodeDataOld = nodeDataMap.at(node_id);
                    if (nodeDataOld.packedGeometryId == -1) {
                        nodeDataOld = nodeData;
                    }
                    else {
                        if (nodeData.nameId != nodeDataOld.nameId || nodeData.packedGeometryId != nodeDataOld.packedGeometryId || nodeData.travelMode != nodeDataOld.travelMode) {
                            SimpleLogger().Write(logWARNING) << "Node (reverse) " << node_id << " data mismatch!";
                            if (nodeData.packedGeometryId != nodeDataOld.packedGeometryId) {
                                std::vector<PointId> geom = getGeometry(nodeData.packedGeometryId);
                                std::vector<PointId> geomOld = getGeometry(nodeDataOld.packedGeometryId);
                                if (geom.size() > geomOld.size()) {
                                    SimpleLogger().Write() << "Replacing geometry...";
                                    nodeDataOld = nodeData;
                                }
                            }
                        }
                        nodeDataOld.weight = std::max(nodeData.weight, nodeDataOld.weight);

                        unsigned int geom_index = nodeData.packedGeometryId;
                        const auto& geometryIndices = _geometryIndices;
                        std::size_t first_index = geometryIndices.at(geom_index);
                        std::size_t last_index = (geom_index + 1 < geometryIndices.size() ? geometryIndices.at(geom_index + 1) : _geometryPointIds.size());
                        bool u_found = std::find(_geometryPointIds.begin() + first_index, _geometryPointIds.begin() + last_index, nodeData.startPointId) != _geometryPointIds.begin() + last_index;
                        if (!u_found) {
                            nodeDataOld.startPointId = nodeData.startPointId;
                        }
                    }
                }
            }
        }

        TIMER_STOP(importSpatialNodes);
        SimpleLogger().Write() << "Importing finished after " << TIMER_SEC(importSpatialNodes) << " seconds";

        SimpleLogger().Write() << "Imported " << nodeDataMap.size() << " nodes" << std::endl;
    }

    void OSRMGraph::importEdges(EdgeDataVector& edgeDataMap) {
        struct OriginalEdgeData {
            unsigned int via_node;
            unsigned int name_id;
            unsigned char turn_instruction;
            bool compressed_geometry;
            unsigned char travel_mode;
        };

        TIMER_START(importEdges);
        SimpleLogger().Write() << "Importing original edges...";

        std::string fileName = _fileName + ".edges";
        std::ifstream file;
        file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        file.open(fileName, std::ios::binary);
        
        unsigned int number_of_edges = 0;
        file.read(reinterpret_cast<char*>(&number_of_edges), sizeof(unsigned int));

        edgeDataMap.reserve(number_of_edges);
        for (unsigned int i = 0; i < number_of_edges; i++) {
            OriginalEdgeData original_edge_data;
            file.read(reinterpret_cast<char*>(&original_edge_data), sizeof(OriginalEdgeData));

            EdgeData edgeData;
            edgeData.turnInstruction = original_edge_data.turn_instruction;
            edgeDataMap.push_back(edgeData);
        }

        TIMER_STOP(importEdges);
        SimpleLogger().Write() << "Importing finished after " << TIMER_SEC(importEdges) << " seconds";

        SimpleLogger().Write() << "Imported " << edgeDataMap.size() << " edges" << std::endl;
    }

    void OSRMGraph::importGraph() {
        static const unsigned char EXPECTED_HEADER_DATA[] = {
            0x4f, 0x53, 0x52, 0x4d, 0x66, 0x31, 0x63, 0x39,
            0x39, 0x30, 0x37, 0x33, 0x35, 0x34, 0x61, 0x65,
            0x35, 0x35, 0x39, 0x30, 0x32, 0x39, 0x39, 0x37,
            0x37, 0x37, 0x63, 0x30, 0x61, 0x31, 0x35, 0x30,
            0x30, 0x64, 0x31, 0x63, 0x00
        };

        struct Header {
            unsigned char data[156];
        };

        struct EdgeEntry {
            unsigned int target;
            unsigned int id : 31;
            bool shortcut : 1;
            int distance : 30;
            bool forward : 1;
            bool backward : 1;
        };

        std::string fileName = _fileName + ".hsgr";
        std::ifstream file;
        file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        file.open(fileName, std::ios::binary);

        Header header;
        file.read(reinterpret_cast<char*>(&header), sizeof(Header));
        if (!std::equal(EXPECTED_HEADER_DATA, EXPECTED_HEADER_DATA + sizeof(EXPECTED_HEADER_DATA), header.data)) {
            SimpleLogger().Write(logWARNING) << "Graph .hsgr header mismatch, wrong OSRM version used when preparing the graph?";
        }
        unsigned int number_of_nodes = 0;
        unsigned int number_of_edges = 0;
        file.read(reinterpret_cast<char*>(&number_of_nodes), sizeof(unsigned int));
        file.read(reinterpret_cast<char*>(&number_of_edges), sizeof(unsigned int));
        
        NodeDataVector nodeDataMap;
        nodeDataMap.reserve(number_of_nodes);
        importNodes(nodeDataMap);
        EdgeDataVector edgeDataMap;
        importEdges(edgeDataMap);

        TIMER_START(importGraph);
        SimpleLogger().Write() << "Importing contracted graph...";

        std::vector<unsigned int> node_list(number_of_nodes);
        file.read(reinterpret_cast<char*>(node_list.data()), sizeof(unsigned int) * number_of_nodes);

        _nodes.reserve(number_of_nodes);
        for (unsigned int i = 0; i + 1 < number_of_nodes; i++) {
            Node node;
            node.firstEdge = node_list[i];
            node.lastEdge = (i + 1 < number_of_nodes ? node_list[i + 1] : number_of_edges);
            if (i >= nodeDataMap.size()) {
                SimpleLogger().Write(logWARNING) << "Node data does not exist for node " << i;
                continue;
            }
            node.nodeData = nodeDataMap.at(i);
            _nodes.push_back(node);
        }

        _edges.reserve(number_of_edges);
        for (unsigned int i = 0; i < number_of_edges; i++) {
            EdgeEntry edge_entry;
            file.read(reinterpret_cast<char*>(&edge_entry), sizeof(EdgeEntry));

            Edge edge;
            edge.targetNodeId = NodeId(edge_entry.target);
            edge.contractedNodeId = edge_entry.shortcut ? NodeId(edge_entry.id) : NodeId(-1);
            edge.weight = edge_entry.distance;
            edge.forward = edge_entry.forward != 0;
            edge.backward = edge_entry.backward != 0;
            if (!edge_entry.shortcut) {
                if (edge_entry.id >= edgeDataMap.size()) {
                    SimpleLogger().Write(logWARNING) << "Edge data does not exist for edge " << edge_entry.id;
                    continue;
                }
                edge.edgeData = edgeDataMap.at(edge_entry.id);
            }
            _edges.push_back(edge);
        }

        TIMER_STOP(importGraph);
        SimpleLogger().Write() << "Importing finished after " << TIMER_SEC(importGraph) << " seconds";

        SimpleLogger().Write() << "Imported " << _nodes.size() << " nodes, " << _edges.size() << " edges" << std::endl;
    }
} }
