#include "PackageSplitter.h"

#include "util/Wgs84.h"

#include "osrm/simple_logger.hpp"
#include "osrm/timing_util.hpp"

namespace Nuti { namespace Routing {
    PackageSplitter::PackageSplitter(const OSRMGraph& graph, const std::map<std::string, std::shared_ptr<PackageTileMask>>& packageTileMaskMap) :
        _graph(graph), _packageTileMaskMap(packageTileMaskMap)
    {
    }

    void PackageSplitter::build() {
        TIMER_START(build);
        SimpleLogger().Write() << "Building package node lists...";

        std::map<std::string, int> packageZoomMap;
        for (auto it = _packageTileMaskMap.begin(); it != _packageTileMaskMap.end(); it++) {
            const std::string& packageId = it->first;
            const PackageTileMask& packageTileMask = *it->second;

            packageZoomMap[packageId] = packageTileMask.getMaxZoomLevel();
        }

        long long nodeCount = _graph.getNodeCount();
        #pragma omp parallel for
        for (long long i = 0; i < nodeCount; i++) {
            NodeId nodeId = static_cast<NodeId>(i);
            std::vector<WMPos> geometry = getNodeGeometry(nodeId);
            for (auto it = _packageTileMaskMap.begin(); it != _packageTileMaskMap.end(); it++) {
                const std::string& packageId = it->first;
                const PackageTileMask& packageTileMask = *it->second;

                bool insidePackage = false;
                int zoom = packageZoomMap.at(packageId);
                int oldX = std::numeric_limits<int>::min();
                int oldY = std::numeric_limits<int>::min();
                for (const WMPos& pos : geometry) {
                    cglib::vec2<double> tilePos = pos + webMecatorBounds();
                    cglib::vec2<double> tileSize = webMecatorBounds() * (2.0 / (1 << zoom));
                    int x = static_cast<int>(std::floor(tilePos(0) / tileSize(0)));
                    int y = static_cast<int>(std::floor(tilePos(1) / tileSize(1)));
                    if (x == oldX && y == oldY) {
                        continue;
                    }
                    if (packageTileMask.getTileStatus(zoom, x, y) != PackageTileStatus::PACKAGE_TILE_STATUS_MISSING) {
                        insidePackage = true;
                        break;
                    }
                    oldX = x;
                    oldY = y;
                }
                if (insidePackage) {
                    std::lock_guard<std::mutex> lock(_packageNodeMapMutex);
                    _packageNodeMap[packageId].push_back(nodeId);
                }
            }
        }

        /*
        for (auto it = _packageNodeMap.begin(); it != _packageNodeMap.end(); it++) {
            std::unordered_set<NodeId> packageNodeIds(it->second.begin(), it->second.end());
            for (std::size_t i = 0; i < it->second.size(); i++) {
                NodeId nodeId = it->second[i];
                const Node& node = _graph.getNode(nodeId);
                for (EdgeIterator ei = node.firstEdge; ei < node.lastEdge; ei++) {
                    const Edge& edge = _graph.getEdge(ei);
                    if (edge.contractedNodeId != -1) {
                        if (packageNodeIds.count(edge.contractedNodeId) == 0) {
                            packageNodeIds.insert(edge.contractedNodeId);
                            it->second.push_back(edge.contractedNodeId);
                        }
                    }
                }
            }
        }
        */

        for (auto it = _packageNodeMap.begin(); it != _packageNodeMap.end(); it++) {
            SimpleLogger().Write() << "Package " << it->first << " contains " << it->second.size() << " nodes";
        }

        TIMER_STOP(build);
        SimpleLogger().Write() << "Building finished after " << TIMER_SEC(build) << " seconds";
    }

    const PackageSplitter::NodeIdVector& PackageSplitter::getPackageNodeIds(const std::string& packageId) const {
        static NodeIdVector emptyNodeIds;
        auto it = _packageNodeMap.find(packageId);
        if (it == _packageNodeMap.end()) {
            return emptyNodeIds;
        }
        return it->second;
    }

    std::vector<PackageSplitter::WMPos> PackageSplitter::getNodeGeometry(NodeId nodeId) const {
        const Node& node = _graph.getNode(nodeId);
        std::vector<WMPos> geometry;
        geometry.push_back(getWMPos(_graph.getPoint(node.nodeData.startPointId)));
        for (PointId pointId : _graph.getGeometry(node.nodeData.packedGeometryId)) {
            geometry.push_back(getWMPos(_graph.getPoint(pointId)));
        }
        if (node.nodeData.geometryReversed) {
            std::reverse(geometry.begin(), geometry.end());
        }
        return geometry;
    }

    PackageSplitter::WMPos PackageSplitter::getWMPos(const Point& point) {
        return wgs84ToWebMercator(cglib::vec2<double>(point.lon * COORDINATE_SCALE, point.lat * COORDINATE_SCALE));
    }
    
    const double PackageSplitter::COORDINATE_SCALE = 1.0e-6;
} }
