#pragma once

#include <utility>

inline void hilbertRot(int n, int *x, int *y, int rx, int ry) {
    if (ry == 0) {
        if (rx == 1) {
            *x = n-1 - *x;
            *y = n-1 - *y;
        }

        int t = *x;
        *x = *y;
        *y = t;
    }
}

inline long long hilbertXY2D (int n, int x, int y) {
    int rx, ry, s;
    long long d=0;
    for (s=n/2; s>0; s/=2) {
        rx = (x & s) > 0;
        ry = (y & s) > 0;
        d += (long long) s * s * ((3 * rx) ^ ry);
        hilbertRot(s, &x, &y, rx, ry);
    }
    return d;
}

inline void hilbertD2XY(int n, long long d, int *x, int *y) {
    int rx, ry, s;
    long long t=d;
    *x = *y = 0;
    for (s=1; s<n; s*=2) {
        rx = 1 & (t/2);
        ry = 1 & (t ^ rx);
        hilbertRot(s, x, y, rx, ry);
        *x += s * rx;
        *y += s * ry;
        t /= 4;
    }
}

