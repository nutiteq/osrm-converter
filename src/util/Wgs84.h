#pragma once

#include <cmath>

#include <cglib/vec.h>

inline cglib::vec2<double> webMecatorBounds() {
    return cglib::vec2<double>(6378137.0 * 3.1415926535897932384626433832795, 6378137.0 * 3.1415926535897932384626433832795);
}

inline cglib::vec2<double> webMercatorToWgs84(const cglib::vec2<double> &mercatorPoint) {
    double x = mercatorPoint(0);
    double y = mercatorPoint(1);
    double num3 = x / 6378137.0;
    double num4 = num3 * 57.295779513082323;
    double num5 = std::floor((double)((num4 + 180.0) / 360.0));
    double num6 = num4 - (num5 * 360.0);
    double num7 = 1.5707963267948966 - (2.0 * std::atan(std::exp((-1.0 * y) / 6378137.0)));
    return cglib::vec2<double>(num6, num7 * 57.295779513082323);
}

inline cglib::vec2<double> wgs84ToWebMercator(const cglib::vec2<double> &wgsPoint) {
    double num = wgsPoint(0) * 0.017453292519943295;
    double x = 6378137.0 * num;
    double a = wgsPoint(1) * 0.017453292519943295;
    double y = 3189068.5 * std::log((1.0 + std::sin(a)) / (1.0 - std::sin(a)));
    return cglib::vec2<double>(x, y);
}
